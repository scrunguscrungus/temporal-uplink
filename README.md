You can download the latest release of Temporal Uplink here:
http://scrungus.club/downloads/temporaluplink/TemporalUplinkLatest.zip

You can find an archive of versions here:
http://scrungus.club/downloads/temporaluplink/archive/

Please note that Temporal Uplink is in a very early state and lacks many features to allow modding at the moment.