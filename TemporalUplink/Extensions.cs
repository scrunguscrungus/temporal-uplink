﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TemporalUplink
{
	public static class TUExtensions
	{

	}

	public static class TUUtil
	{
		static TextBoxWriter debugListen = new TextBoxWriter( null );

		static public void SetupDebugListener( TextBox targetbox )
		{
			if ( !Debug.Listeners.Contains( debugListen ) )
				Debug.Listeners.Add( debugListen );
			targetbox.Clear( );
			debugListen.MyControl = targetbox;
		}
	}

	public class TextBoxWriter : TraceListener
	{
		public TextBox MyControl;

		delegate void AddTextCallback( string text );

		private void AddText( string text )
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if ( this.MyControl.InvokeRequired )
			{
				AddTextCallback d = new AddTextCallback( AddText );
				this.MyControl.Invoke( d, new object[ ] { text } );
			}
			else
			{
				this.MyControl.AppendText( text );
			}
		}

		public TextBoxWriter( TextBox control )
		{
			MyControl = control;
		}

		/*
		public override void Write( char value )
		{
			AddText( value.ToString() );
		}*/

		public override void Write( string value )
		{
			AddText( value );
		}
		public override void WriteLine( string msg )
		{
			Write( msg + Environment.NewLine );
		}
	}
}
