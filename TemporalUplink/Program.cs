﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;

namespace TemporalUplink
{
    static class Program
    {
		
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

			Application.Run( new TemporalUplink.Forms.Splash( ) );
			Application.Run( new MainWindow( ) );
			
        }

		
    }
}
