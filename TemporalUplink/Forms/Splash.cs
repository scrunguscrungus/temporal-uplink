﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TemporalUplink.Forms
{
	public partial class Splash : Form
	{
		

		public Splash( )
		{
			BringToFront( );
			string[ ] splashMessages = new string[ ] {
				"Get your melons out for the monkeys!",
				"Have you played as Pulov Yuran yet?",
				"Humankind's genetic superior!",
				"Woah, Anya, did you see that?",
				"The crystals are the key!",
				"Powered by the time crystals!",
				"Damnit!",
				"Sponsored by U-Genix Corporation!",
				//"I'm you!",
				//"You're me?"
			};
			InitializeComponent( );
			lblStartText.Text = splashMessages[ new Random( ).Next( 0, splashMessages.Length - 1 ) ];
			if ( Properties.Settings.Default.FirstRun )
			{
				if ( MessageBox.Show("This appears to be your first time running Temporal Uplink. Would you like to view the online documentation?", "First Run", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
				{
					System.Diagnostics.Process.Start( Properties.Resources.helpURL ); //Temp
				}
				Properties.Settings.Default.FirstRun = false;
				Properties.Settings.Default.Save( );
			}
			
			Task.Run( () => Wait() );
			
		}
		private void CloseSplash()
		{
			if ( InvokeRequired )
			{
				Invoke( new MethodInvoker( delegate { Close(); } ) );
			}
		}

		private void Wait()
		{
			System.Threading.Thread.Sleep( 3000 );
			CloseSplash( );
		}
	}
}
