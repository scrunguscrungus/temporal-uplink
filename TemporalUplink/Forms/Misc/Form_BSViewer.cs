﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3BSLib;
using System.IO;

namespace TemporalUplink.Forms
{
	public partial class Form_BSViewer : Form
	{
		TS3BS loadedBS;

		OpenFileDialog dlgOpenBS;
		SaveFileDialog dlgSaveBS;
		SaveFileDialog dlgExportBS;

		OpenFileDialog dlgOpenTXT;
		public Form_BSViewer()
		{
			InitializeComponent();

			dlgOpenBS = new OpenFileDialog();
			dlgOpenBS.Filter = "TS3 BS Files|*.bs";

			dlgSaveBS = new SaveFileDialog();
			dlgSaveBS.Filter = dlgOpenBS.Filter;

			dlgExportBS = new SaveFileDialog();
			dlgExportBS.Filter = "Text File|*.txt";

			dlgOpenTXT = new OpenFileDialog();
			dlgOpenTXT.Filter = dlgExportBS.Filter;

			MainMenu menu = new MainMenu();

			MenuItem fileItem = new MenuItem("&File");
			MenuItem fileOpenItem = new MenuItem("&Open");
			MenuItem fileSaveItem = new MenuItem("&Save");
			MenuItem fileImportItem = new MenuItem("&Import");
			MenuItem fileExportItem = new MenuItem("&Export");

			fileOpenItem.Click += FileOpenItem_Click;
			fileSaveItem.Click += FileSaveItem_Click;
			fileImportItem.Click += FileImportItem_Click;
			fileExportItem.Click += FileExportItem_Click;

			fileItem.MenuItems.Add(fileOpenItem);
			fileItem.MenuItems.Add(fileSaveItem);
			fileItem.MenuItems.Add(fileImportItem);
			fileItem.MenuItems.Add(fileExportItem);
			menu.MenuItems.Add(fileItem);

			Menu = menu;
		}

		private void FileImportItem_Click(object sender, EventArgs e)
		{
			loadedBS = null;
			
			if ( dlgOpenTXT.ShowDialog() == DialogResult.OK )
			{
				stringList.Items.Clear();

				loadedBS = TS3BS.ConstructFromTXT(dlgOpenTXT.FileName);

				for (int i = 0; i < loadedBS.Entries.Count; i++)
				{
					stringList.Items.Add(loadedBS.Entries[i]);
				}
			}
		}

		private void FileSaveItem_Click(object sender, EventArgs e)
		{
			if (loadedBS == null)
			{
				MessageBox.Show("Nothing to save!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if ( dlgSaveBS.ShowDialog() == DialogResult.OK )
			{
				loadedBS.WriteToFile(dlgSaveBS.FileName);
			}
		}

		private void FileExportItem_Click(object sender, EventArgs e)
		{
			if ( loadedBS == null )
			{
				MessageBox.Show("Nothing to export!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if ( dlgExportBS.ShowDialog() == DialogResult.OK )
			{
				using (StreamWriter sw = new StreamWriter(File.OpenWrite(dlgExportBS.FileName)))
				{
					foreach (string entry in loadedBS.Entries)
					{
						sw.WriteLine(entry.Trim('\0').Replace("\n", "\\n"));
					}
				}
			}
		}

		private void FileOpenItem_Click(object sender, EventArgs e)
		{
			if ( dlgOpenBS.ShowDialog() == DialogResult.OK )
			{
				stringList.Items.Clear();

				loadedBS = TS3BS.ReadFromFile(dlgOpenBS.FileName);

				for ( int i=0; i<loadedBS.Entries.Count; i++)
				{
					stringList.Items.Add(loadedBS.Entries[i]);
				}

				dlgExportBS.FileName = Path.GetFileNameWithoutExtension(dlgOpenBS.FileName);
			}
		}
	}
}
