﻿/*namespace TemporalUplink.Forms
{
	partial class Form_MiscIdentify
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( )
		{
			this.btnBrowseInput = new System.Windows.Forms.Button();
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.txtInput = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnIdentify = new System.Windows.Forms.Button();
			this.openInputDialog = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// btnBrowseInput
			// 
			this.btnBrowseInput.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			this.btnBrowseInput.Location = new System.Drawing.Point(492, 46);
			this.btnBrowseInput.Name = "btnBrowseInput";
			this.btnBrowseInput.Size = new System.Drawing.Size(47, 23);
			this.btnBrowseInput.TabIndex = 18;
			this.btnBrowseInput.UseVisualStyleBackColor = true;
			this.btnBrowseInput.Click += new System.EventHandler(this.btnBrowseInput_Click);
			// 
			// txtStatus
			// 
			this.txtStatus.BackColor = System.Drawing.Color.Black;
			this.txtStatus.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStatus.ForeColor = System.Drawing.Color.LimeGreen;
			this.txtStatus.Location = new System.Drawing.Point(14, 144);
			this.txtStatus.Multiline = true;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.ReadOnly = true;
			this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtStatus.Size = new System.Drawing.Size(526, 165);
			this.txtStatus.TabIndex = 11;
			// 
			// txtInput
			// 
			this.txtInput.Location = new System.Drawing.Point(81, 49);
			this.txtInput.Name = "txtInput";
			this.txtInput.Size = new System.Drawing.Size(406, 20);
			this.txtInput.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(23, 13);
			this.label1.TabIndex = 15;
			this.label1.Text = "File";
			// 
			// btnIdentify
			// 
			this.btnIdentify.Location = new System.Drawing.Point(14, 115);
			this.btnIdentify.Name = "btnIdentify";
			this.btnIdentify.Size = new System.Drawing.Size(525, 23);
			this.btnIdentify.TabIndex = 21;
			this.btnIdentify.Text = "Identify";
			this.btnIdentify.UseVisualStyleBackColor = true;
			this.btnIdentify.Click += new System.EventHandler(this.btnIdentify_Click);
			// 
			// openInputDialog
			// 
			this.openInputDialog.Filter = "Unknown Files|*.*";
			// 
			// Form_MiscIdentify
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(551, 321);
			this.Controls.Add(this.btnIdentify);
			this.Controls.Add(this.btnBrowseInput);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtInput);
			this.Controls.Add(this.txtStatus);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form_MiscIdentify";
			this.Text = "Identify File";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnBrowseInput;
		private System.Windows.Forms.TextBox txtStatus;
		private System.Windows.Forms.TextBox txtInput;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnIdentify;
		private System.Windows.Forms.OpenFileDialog openInputDialog;

	}
}*/