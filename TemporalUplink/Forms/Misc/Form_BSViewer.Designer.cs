﻿namespace TemporalUplink.Forms
{
	partial class Form_BSViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.stringList = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// stringList
			// 
			this.stringList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.stringList.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.stringList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.stringList.ForeColor = System.Drawing.SystemColors.Menu;
			this.stringList.FormattingEnabled = true;
			this.stringList.HorizontalScrollbar = true;
			this.stringList.Location = new System.Drawing.Point(13, 13);
			this.stringList.Name = "stringList";
			this.stringList.Size = new System.Drawing.Size(572, 431);
			this.stringList.TabIndex = 0;
			// 
			// Form_BSViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(597, 471);
			this.Controls.Add(this.stringList);
			this.Name = "Form_BSViewer";
			this.Text = "BS Viewer";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox stringList;
	}
}