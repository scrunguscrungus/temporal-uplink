﻿namespace TemporalUplink.Forms
{
    partial class Form_PAKFromC2N
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.txtC2N = new System.Windows.Forms.TextBox();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowseC2N = new System.Windows.Forms.Button();
            this.btnBrowseInput = new System.Windows.Forms.Button();
            this.btnBrowseOutput = new System.Windows.Forms.Button();
            this.openC2NDialog = new System.Windows.Forms.OpenFileDialog();
            this.savePAKDialog = new System.Windows.Forms.SaveFileDialog();
            this.btnBuild = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.Window;
            this.txtStatus.Location = new System.Drawing.Point(13, 144);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStatus.Size = new System.Drawing.Size(526, 165);
            this.txtStatus.TabIndex = 0;
            // 
            // txtC2N
            // 
            this.txtC2N.Location = new System.Drawing.Point(79, 12);
            this.txtC2N.Name = "txtC2N";
            this.txtC2N.Size = new System.Drawing.Size(406, 20);
            this.txtC2N.TabIndex = 1;
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(79, 47);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(406, 20);
            this.txtInput.TabIndex = 2;
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(79, 84);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(406, 20);
            this.txtOutput.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "C2N";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Input Folder";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Output File";
            // 
            // btnBrowseC2N
            // 
            this.btnBrowseC2N.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
            this.btnBrowseC2N.Location = new System.Drawing.Point(492, 12);
            this.btnBrowseC2N.Name = "btnBrowseC2N";
            this.btnBrowseC2N.Size = new System.Drawing.Size(47, 23);
            this.btnBrowseC2N.TabIndex = 7;
            this.btnBrowseC2N.UseVisualStyleBackColor = true;
            this.btnBrowseC2N.Click += new System.EventHandler(this.btnBrowseC2N_Click);
            // 
            // btnBrowseInput
            // 
            this.btnBrowseInput.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
            this.btnBrowseInput.Location = new System.Drawing.Point(491, 47);
            this.btnBrowseInput.Name = "btnBrowseInput";
            this.btnBrowseInput.Size = new System.Drawing.Size(47, 23);
            this.btnBrowseInput.TabIndex = 8;
            this.btnBrowseInput.UseVisualStyleBackColor = true;
            this.btnBrowseInput.Click += new System.EventHandler(this.btnBrowseInput_Click);
            // 
            // btnBrowseOutput
            // 
            this.btnBrowseOutput.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
            this.btnBrowseOutput.Location = new System.Drawing.Point(491, 84);
            this.btnBrowseOutput.Name = "btnBrowseOutput";
            this.btnBrowseOutput.Size = new System.Drawing.Size(47, 23);
            this.btnBrowseOutput.TabIndex = 9;
            this.btnBrowseOutput.UseVisualStyleBackColor = true;
            this.btnBrowseOutput.Click += new System.EventHandler(this.btnBrowseOutput_Click);
            // 
            // openC2NDialog
            // 
            this.openC2NDialog.DefaultExt = "c2n";
            this.openC2NDialog.Filter = "TS3 C2N Files|*.c2n";
            // 
            // savePAKDialog
            // 
            this.savePAKDialog.DefaultExt = "pak";
            this.savePAKDialog.Filter = "TS3 PAK File|*.pak";
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(13, 115);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(525, 23);
            this.btnBuild.TabIndex = 10;
            this.btnBuild.Text = "Build";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // Form_PAKFromC2N
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 321);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.btnBrowseOutput);
            this.Controls.Add(this.btnBrowseInput);
            this.Controls.Add(this.btnBrowseC2N);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.txtC2N);
            this.Controls.Add(this.txtStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_PAKFromC2N";
            this.Text = "Create PAK from C2N";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.TextBox txtC2N;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBrowseC2N;
        private System.Windows.Forms.Button btnBrowseInput;
        private System.Windows.Forms.Button btnBrowseOutput;
        private System.Windows.Forms.OpenFileDialog openC2NDialog;
        private System.Windows.Forms.SaveFileDialog savePAKDialog;
        private System.Windows.Forms.Button btnBuild;
    }
}