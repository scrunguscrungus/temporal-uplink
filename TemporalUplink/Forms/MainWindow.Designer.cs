﻿namespace TemporalUplink
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnPAKInfo = new System.Windows.Forms.Button();
			this.btnHelp = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPagePAKTools = new System.Windows.Forms.TabPage();
			this.btnBrowsePAK = new System.Windows.Forms.Button();
			this.tabPageLevelData = new System.Windows.Forms.TabPage();
			this.tabPageModelsTex = new System.Windows.Forms.TabPage();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.btnGCRInfo = new System.Windows.Forms.Button();
			this.tabPageStrings = new System.Windows.Forms.TabPage();
			this.btnBSBrowser = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPagePAKTools.SuspendLayout();
			this.tabPageModelsTex.SuspendLayout();
			this.tabPageStrings.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnPAKInfo
			// 
			this.btnPAKInfo.BackColor = System.Drawing.SystemColors.HotTrack;
			this.btnPAKInfo.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnPAKInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPAKInfo.Location = new System.Drawing.Point(3, 3);
			this.btnPAKInfo.Name = "btnPAKInfo";
			this.btnPAKInfo.Size = new System.Drawing.Size(75, 23);
			this.btnPAKInfo.TabIndex = 0;
			this.btnPAKInfo.Text = "PAK Info";
			this.btnPAKInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.btnPAKInfo.UseVisualStyleBackColor = false;
			this.btnPAKInfo.Click += new System.EventHandler(this.btnPAKInfo_Click);
			// 
			// btnHelp
			// 
			this.btnHelp.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnHelp.Location = new System.Drawing.Point(747, 7);
			this.btnHelp.Name = "btnHelp";
			this.btnHelp.Size = new System.Drawing.Size(23, 20);
			this.btnHelp.TabIndex = 5;
			this.btnHelp.Text = "?";
			this.btnHelp.UseVisualStyleBackColor = true;
			this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPagePAKTools);
			this.tabControl1.Controls.Add(this.tabPageLevelData);
			this.tabControl1.Controls.Add(this.tabPageModelsTex);
			this.tabControl1.Controls.Add(this.tabPageStrings);
			this.tabControl1.HotTrack = true;
			this.tabControl1.Location = new System.Drawing.Point(6, 7);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.ShowToolTips = true;
			this.tabControl1.Size = new System.Drawing.Size(764, 480);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPagePAKTools
			// 
			this.tabPagePAKTools.BackColor = System.Drawing.SystemColors.Control;
			this.tabPagePAKTools.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.tabPagePAKTools.Controls.Add(this.btnBrowsePAK);
			this.tabPagePAKTools.Controls.Add(this.btnPAKInfo);
			this.tabPagePAKTools.Location = new System.Drawing.Point(4, 22);
			this.tabPagePAKTools.Name = "tabPagePAKTools";
			this.tabPagePAKTools.Size = new System.Drawing.Size(756, 454);
			this.tabPagePAKTools.TabIndex = 0;
			this.tabPagePAKTools.Text = "PAK Tools";
			this.tabPagePAKTools.ToolTipText = "Tools for working with PAK archives";
			// 
			// btnBrowsePAK
			// 
			this.btnBrowsePAK.Location = new System.Drawing.Point(3, 32);
			this.btnBrowsePAK.Name = "btnBrowsePAK";
			this.btnBrowsePAK.Size = new System.Drawing.Size(84, 23);
			this.btnBrowsePAK.TabIndex = 6;
			this.btnBrowsePAK.Text = "PAK Browser";
			this.btnBrowsePAK.UseVisualStyleBackColor = true;
			this.btnBrowsePAK.Click += new System.EventHandler(this.btnBrowsePAK_Click);
			// 
			// tabPageLevelData
			// 
			this.tabPageLevelData.BackColor = System.Drawing.SystemColors.Control;
			this.tabPageLevelData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.tabPageLevelData.Location = new System.Drawing.Point(4, 22);
			this.tabPageLevelData.Name = "tabPageLevelData";
			this.tabPageLevelData.Size = new System.Drawing.Size(756, 454);
			this.tabPageLevelData.TabIndex = 0;
			this.tabPageLevelData.Text = "Level Data";
			this.tabPageLevelData.ToolTipText = "Tools for viewing and manipulating level data";
			// 
			// tabPageModelsTex
			// 
			this.tabPageModelsTex.BackColor = System.Drawing.SystemColors.Control;
			this.tabPageModelsTex.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.tabPageModelsTex.Controls.Add(this.button2);
			this.tabPageModelsTex.Controls.Add(this.button1);
			this.tabPageModelsTex.Controls.Add(this.btnGCRInfo);
			this.tabPageModelsTex.Location = new System.Drawing.Point(4, 22);
			this.tabPageModelsTex.Name = "tabPageModelsTex";
			this.tabPageModelsTex.Padding = new System.Windows.Forms.Padding(3);
			this.tabPageModelsTex.Size = new System.Drawing.Size(756, 454);
			this.tabPageModelsTex.TabIndex = 1;
			this.tabPageModelsTex.Text = "Models and Textures";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(7, 65);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 2;
			this.button2.Text = "GCT Viewer";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.BtnGCTView_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(6, 35);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "GCT Info";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.BtnGCTInfo_Click);
			// 
			// btnGCRInfo
			// 
			this.btnGCRInfo.Location = new System.Drawing.Point(6, 6);
			this.btnGCRInfo.Name = "btnGCRInfo";
			this.btnGCRInfo.Size = new System.Drawing.Size(75, 23);
			this.btnGCRInfo.TabIndex = 0;
			this.btnGCRInfo.Text = "GCR Info";
			this.btnGCRInfo.UseVisualStyleBackColor = true;
			this.btnGCRInfo.Click += new System.EventHandler(this.BtnGCRInfo_Click);
			// 
			// tabPageStrings
			// 
			this.tabPageStrings.BackColor = System.Drawing.SystemColors.Control;
			this.tabPageStrings.Controls.Add(this.btnBSBrowser);
			this.tabPageStrings.Location = new System.Drawing.Point(4, 22);
			this.tabPageStrings.Name = "tabPageStrings";
			this.tabPageStrings.Size = new System.Drawing.Size(756, 454);
			this.tabPageStrings.TabIndex = 2;
			this.tabPageStrings.Text = "Strings";
			// 
			// btnBSBrowser
			// 
			this.btnBSBrowser.Location = new System.Drawing.Point(4, 4);
			this.btnBSBrowser.Name = "btnBSBrowser";
			this.btnBSBrowser.Size = new System.Drawing.Size(75, 23);
			this.btnBSBrowser.TabIndex = 0;
			this.btnBSBrowser.Text = "BS Viewer";
			this.btnBSBrowser.UseVisualStyleBackColor = true;
			this.btnBSBrowser.Click += new System.EventHandler(this.btnBSBrowser_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(782, 499);
			this.Controls.Add(this.btnHelp);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "MainWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Temporal Uplink";
			this.tabControl1.ResumeLayout(false);
			this.tabPagePAKTools.ResumeLayout(false);
			this.tabPageModelsTex.ResumeLayout(false);
			this.tabPageStrings.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Button btnPAKInfo;
		private System.Windows.Forms.Button btnHelp;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPagePAKTools;
		private System.Windows.Forms.TabPage tabPageLevelData;
		private System.Windows.Forms.TabPage tabPageModelsTex;
		private System.Windows.Forms.Button btnGCRInfo;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button btnBrowsePAK;
		private System.Windows.Forms.TabPage tabPageStrings;
		private System.Windows.Forms.Button btnBSBrowser;
	}
}

