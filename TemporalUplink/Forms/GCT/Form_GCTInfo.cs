﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3GCTLib;

namespace TemporalUplink.Forms
{
	public partial class Form_GCTInfo : Form
	{
		private TS3GCT currGCT;
		public Form_GCTInfo()
		{
			InitializeComponent();
		}


		private void TxtInfo_KeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = true;
			return;
		}

		private void BtnFindGCT_Click(object sender, EventArgs e)
		{
			txtInfo.Text = "Loading...";
			if (dlgFindGCR.ShowDialog() == DialogResult.OK)
			{
				try
				{
					currGCT = new TS3GCT(dlgFindGCR.FileName);
				}
				catch (Exception exception)
				{
					MessageBox.Show("The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
					return;
				}

				UpdateStats();

			}
		}
		private void UpdateStats()
		{
			if (currGCT == null)
				return;

			txtInfo.Text = "";

			txtInfo.Text += "GCT: " + Path.GetFileName(dlgFindGCR.FileName);
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Target Width?: " + currGCT.width1;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Target Height?: " + currGCT.height1;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Actual Width: " + currGCT.width2;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Actual Height: " + currGCT.height2;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Flags?: " + Convert.ToString((int)currGCT.texFlags, 2).PadLeft(32, '0');
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Unknown: " + currGCT.unknown;
			txtInfo.Text += Environment.NewLine;

		}
	}
}
