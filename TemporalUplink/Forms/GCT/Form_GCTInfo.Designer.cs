﻿namespace TemporalUplink.Forms
{
	partial class Form_GCTInfo
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtInfo = new System.Windows.Forms.TextBox();
			this.btnFindGCT = new System.Windows.Forms.Button();
			this.lblGCRINfo = new System.Windows.Forms.Label();
			this.lblPAK = new System.Windows.Forms.Label();
			this.dlgFindGCR = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// txtInfo
			// 
			this.txtInfo.BackColor = System.Drawing.SystemColors.Window;
			this.txtInfo.Location = new System.Drawing.Point(12, 26);
			this.txtInfo.Multiline = true;
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.ReadOnly = true;
			this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtInfo.Size = new System.Drawing.Size(524, 185);
			this.txtInfo.TabIndex = 13;
			this.txtInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtInfo_KeyDown);
			// 
			// btnFindGCT
			// 
			this.btnFindGCT.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			this.btnFindGCT.Location = new System.Drawing.Point(497, 230);
			this.btnFindGCT.Name = "btnFindGCT";
			this.btnFindGCT.Size = new System.Drawing.Size(32, 23);
			this.btnFindGCT.TabIndex = 12;
			this.btnFindGCT.UseVisualStyleBackColor = true;
			this.btnFindGCT.Click += new System.EventHandler(this.BtnFindGCT_Click);
			// 
			// lblGCRINfo
			// 
			this.lblGCRINfo.AutoSize = true;
			this.lblGCRINfo.Location = new System.Drawing.Point(9, 7);
			this.lblGCRINfo.Name = "lblGCRINfo";
			this.lblGCRINfo.Size = new System.Drawing.Size(72, 13);
			this.lblGCRINfo.TabIndex = 11;
			this.lblGCRINfo.Text = "GCT File Info:";
			// 
			// lblPAK
			// 
			this.lblPAK.AutoSize = true;
			this.lblPAK.Location = new System.Drawing.Point(494, 214);
			this.lblPAK.Name = "lblPAK";
			this.lblPAK.Size = new System.Drawing.Size(32, 13);
			this.lblPAK.TabIndex = 14;
			this.lblPAK.Text = "GCT:";
			// 
			// dlgFindGCR
			// 
			this.dlgFindGCR.DefaultExt = "pak";
			this.dlgFindGCR.Filter = "TS3 GCT|*.gct";
			// 
			// Form_GCTInfo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(564, 264);
			this.Controls.Add(this.txtInfo);
			this.Controls.Add(this.btnFindGCT);
			this.Controls.Add(this.lblGCRINfo);
			this.Controls.Add(this.lblPAK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form_GCTInfo";
			this.Text = "GCT Info";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtInfo;
		private System.Windows.Forms.Button btnFindGCT;
		private System.Windows.Forms.Label lblGCRINfo;
		private System.Windows.Forms.Label lblPAK;
		private System.Windows.Forms.OpenFileDialog dlgFindGCR;
	}
}