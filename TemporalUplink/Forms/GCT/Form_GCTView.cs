﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3GCTLib;

namespace TemporalUplink.Forms
{
	public partial class Form_GCTView : Form
	{
		string exportName;

		private MainMenu mainMenu;
		private MenuItem flipImageVertically;

		private List<FlagLabel> flagLabels = new List<FlagLabel>();

		string filePath;
		TS3GCT gctFile;
		Bitmap baseImage;

		FileBrowser browser;

		public Form_GCTView()
		{
			InitializeComponent();
			InvalidateLoadedFile();
			mainMenu = new MainMenu();
			MenuItem File = mainMenu.MenuItems.Add("&File");
			MenuItem openItem = new MenuItem("&Open");
			MenuItem exportItem = new MenuItem("&Export...");
			MenuItem saveFlags = new MenuItem("&Save Flags");

			MenuItem Options = mainMenu.MenuItems.Add("&Options");
			flipImageVertically = new MenuItem("&Flip image vertically");

			File.MenuItems.Add(openItem);
			File.MenuItems.Add(exportItem);
			File.MenuItems.Add(saveFlags);

			Options.MenuItems.Add(flipImageVertically);

			openItem.Click += btnFindGCT_Click;
			exportItem.Click += btnSave_Click;
			saveFlags.Click += SaveFlags;

			flipImageVertically.Click += toggleFlipImage;

			this.Menu = mainMenu;

			browser = new FileBrowser();

			flipImageVertically.Checked = Properties.Settings.Default.GCTViewer_FlipImg;
		}

		private void SaveFlags(object sender, EventArgs e)
		{
			if ( gctFile == null )
			{
				MessageBox.Show(this, "You can't save nothing!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			int newFlags = 0;
			foreach (FlagLabel flagLabel in flagLabels)
			{
				if ( flagLabel.bActive )
				{
					newFlags += (int)flagLabel.linkedFlag;
				}
			}

			gctFile.SaveFlags(newFlags);
		}

		private void LoadGCT(string gctPath)
		{
			filePath = gctPath;
			gctFile = new TS3GCT(gctPath);
			baseImage = gctFile.bitmap;
			imgGCT.Image = baseImage;
			CheckImageFlip();
			exportName = System.IO.Path.GetFileNameWithoutExtension(gctPath);
			string infoText = @"Filename: {0}
			File Resolution: {1}x{2}
			Ingame Resolution: {3}x{4}
			Unknown Value: {5}";

			foreach(Label flagLabel in flagLabels)
			{
				infoBox.Controls.Remove(flagLabel);
			}
			flagLabels.Clear();

			int i = 1;
			foreach (TS3GCT.GCT_Flag flag in Enum.GetValues(typeof(TS3GCT.GCT_Flag)))
			{
				
				if (flag == TS3GCT.GCT_Flag.FLAG_NONE)
				{
					continue;
				}

				FlagLabel newLabel = new FlagLabel();
				newLabel.linkedFlag = flag;
				flagLabels.Add(newLabel);

				infoBox.SuspendLayout();
				infoBox.Controls.Add(newLabel);
				infoBox.ResumeLayout();

				newLabel.Text = Enum.GetName(typeof(TS3GCT.GCT_Flag), flag);

				if ((gctFile.texFlags & flag) != 0)
				{
					newLabel.bActive = true;
				}

				i++;
			}
			


			lblFileInfo.Text = String.Format(infoText, System.IO.Path.GetFileName(gctPath), gctFile.width2, gctFile.height2, gctFile.width1, gctFile.height1, gctFile.unknown, Convert.ToString((int)gctFile.texFlags, 2).PadLeft(32, '0'));
		}

		private void FailGCTLoad()
		{
			MessageBox.Show(this, "The specified file could not be loaded as a GCT.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			InvalidateLoadedFile();
		}

		private void CheckImageFlip()
		{
			if (baseImage == null)
				return;
			Bitmap temp = (Bitmap)baseImage.Clone();
			if (Properties.Settings.Default.GCTViewer_FlipImg)
			{
				temp.RotateFlip(RotateFlipType.RotateNoneFlipY);
			}
			else
			{
				temp.RotateFlip(RotateFlipType.RotateNoneFlipNone);
			}
			imgGCT.Image = temp;
		}

		private void toggleFlipImage(object sender, EventArgs e)
		{
			flipImageVertically.Checked = !flipImageVertically.Checked;
			Properties.Settings.Default.GCTViewer_FlipImg = flipImageVertically.Checked;
			Properties.Settings.Default.Save();
			CheckImageFlip();

		}

		private void btnFindGCT_Click(object sender, EventArgs e)
		{
			if ( browser.ShowDialog() == DialogResult.OK)
			{
				try
				{
					LoadGCT(browser.FilePath);
				}
				catch
				{
					FailGCTLoad();
				}
			}
		}

		private void Browser_FormClosed(object sender, FormClosedEventArgs e)
		{
			browser = null;
		}

		private void InvalidateLoadedFile()
		{
			imgGCT.Image = null;
			lblFileInfo.Text = "";
			foreach (Label flagLabel in flagLabels)
			{
				infoBox.Controls.Remove(flagLabel);
			}
			flagLabels.Clear();
			gctFile = null;
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (imgGCT.Image == null)
			{
				MessageBox.Show(this, "You can't export nothing!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if (dlgSavePNG.FileName != exportName)
				dlgSavePNG.FileName = exportName;

			if (dlgSavePNG.ShowDialog() == DialogResult.OK)
			{
				System.Drawing.Imaging.ImageFormat targetOutFormat;
				switch (System.IO.Path.GetExtension(dlgSavePNG.FileName).ToUpperInvariant())
				{
					case ".PNG":
						targetOutFormat = System.Drawing.Imaging.ImageFormat.Png;
						break;
					case ".BMP":
						targetOutFormat = System.Drawing.Imaging.ImageFormat.Bmp;
						break;
					case ".JPG":
					case ".JPEG":
						targetOutFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
						break;
					default:
						targetOutFormat = System.Drawing.Imaging.ImageFormat.Png;
						break;
				}

				imgGCT.Image.Save(dlgSavePNG.FileName, targetOutFormat);
			}
		}

		private void Form_GCTView_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
		}

		private void Form_GCTView_DragDrop(object sender, DragEventArgs e)
		{
			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
			try
			{
				LoadGCT(files[0]);
			}
			catch
			{
				FailGCTLoad();
			}
		}
	}
	class FlagLabel : Label
	{
		public TS3GCT.GCT_Flag linkedFlag = TS3GCT.GCT_Flag.FLAG_NONE;
		public bool bActive = false;

		override protected void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			UpdateColour();
		}

		protected override void OnClick(EventArgs e)
		{
			base.OnClick(e);
			bActive = !bActive;
			Refresh();
		}

		void UpdateColour()
		{
			if (bActive)
			{
				BackColor = Color.PaleGreen;
			}
			else
			{
				BackColor = Color.IndianRed;
			}
		}
	}
}
