﻿namespace TemporalUplink.Forms
{
	partial class Form_GCTView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imgGCT = new System.Windows.Forms.PictureBox();
			this.dlgReadGCT = new System.Windows.Forms.OpenFileDialog();
			this.dlgSavePNG = new System.Windows.Forms.SaveFileDialog();
			this.infoBox = new System.Windows.Forms.FlowLayoutPanel();
			this.lblFileInfo = new System.Windows.Forms.Label();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			((System.ComponentModel.ISupportInitialize)(this.imgGCT)).BeginInit();
			this.infoBox.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// imgGCT
			// 
			this.imgGCT.Location = new System.Drawing.Point(3, 3);
			this.imgGCT.Name = "imgGCT";
			this.imgGCT.Size = new System.Drawing.Size(67, 57);
			this.imgGCT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.imgGCT.TabIndex = 0;
			this.imgGCT.TabStop = false;
			// 
			// dlgReadGCT
			// 
			this.dlgReadGCT.Filter = "TS3 GCT|*.gct";
			// 
			// dlgSavePNG
			// 
			this.dlgSavePNG.DefaultExt = "png";
			this.dlgSavePNG.Filter = "PNG Files|*.png|BMP Files|*.bmp|JPG Files|*.jpg";
			// 
			// infoBox
			// 
			this.infoBox.AutoSize = true;
			this.infoBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.infoBox.Controls.Add(this.lblFileInfo);
			this.infoBox.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.infoBox.Location = new System.Drawing.Point(13, 12);
			this.infoBox.Name = "infoBox";
			this.infoBox.Size = new System.Drawing.Size(281, 795);
			this.infoBox.TabIndex = 1;
			// 
			// lblFileInfo
			// 
			this.lblFileInfo.AutoSize = true;
			this.lblFileInfo.Location = new System.Drawing.Point(3, 0);
			this.lblFileInfo.Name = "lblFileInfo";
			this.lblFileInfo.Size = new System.Drawing.Size(0, 13);
			this.lblFileInfo.TabIndex = 0;
			this.lblFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblFileInfo.UseMnemonic = false;
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel2.AutoSize = true;
			this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.flowLayoutPanel2.Controls.Add(this.imgGCT);
			this.flowLayoutPanel2.Location = new System.Drawing.Point(300, 12);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(718, 795);
			this.flowLayoutPanel2.TabIndex = 2;
			// 
			// Form_GCTView
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1030, 819);
			this.Controls.Add(this.flowLayoutPanel2);
			this.Controls.Add(this.infoBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "Form_GCTView";
			this.Text = "GCT Viewer";
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form_GCTView_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form_GCTView_DragEnter);
			((System.ComponentModel.ISupportInitialize)(this.imgGCT)).EndInit();
			this.infoBox.ResumeLayout(false);
			this.infoBox.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox imgGCT;
		private System.Windows.Forms.OpenFileDialog dlgReadGCT;
		private System.Windows.Forms.SaveFileDialog dlgSavePNG;
		private System.Windows.Forms.FlowLayoutPanel infoBox;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Label lblFileInfo;
	}
}