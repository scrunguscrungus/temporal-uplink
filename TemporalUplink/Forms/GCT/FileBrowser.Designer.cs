﻿namespace TemporalUplink.Forms
{
	partial class FileBrowser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( )
		{
			this.fileList = new System.Windows.Forms.ListView();
			this.colFilename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.txtFolder = new System.Windows.Forms.TextBox();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.txtFilePath = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.ddViewMode = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// fileList
			// 
			this.fileList.AllowColumnReorder = true;
			this.fileList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.fileList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFilename,
            this.colSize});
			this.fileList.GridLines = true;
			this.fileList.Location = new System.Drawing.Point(13, 63);
			this.fileList.MultiSelect = false;
			this.fileList.Name = "fileList";
			this.fileList.Size = new System.Drawing.Size(568, 343);
			this.fileList.TabIndex = 0;
			this.fileList.UseCompatibleStateImageBehavior = false;
			this.fileList.ItemActivate += new System.EventHandler(this.fileList_ItemActivate);
			this.fileList.SelectedIndexChanged += new System.EventHandler(this.fileList_SelectedIndexChanged);
			// 
			// colFilename
			// 
			this.colFilename.Text = "File";
			this.colFilename.Width = 268;
			// 
			// colSize
			// 
			this.colSize.Text = "Size";
			this.colSize.Width = 268;
			// 
			// txtFolder
			// 
			this.txtFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtFolder.Location = new System.Drawing.Point(13, 10);
			this.txtFolder.Name = "txtFolder";
			this.txtFolder.Size = new System.Drawing.Size(487, 20);
			this.txtFolder.TabIndex = 1;
			this.txtFolder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFolder_Enter);
			// 
			// btnBrowse
			// 
			this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowse.Location = new System.Drawing.Point(506, 10);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(75, 23);
			this.btnBrowse.TabIndex = 2;
			this.btnBrowse.Text = "Browse";
			this.btnBrowse.UseVisualStyleBackColor = true;
			this.btnBrowse.Click += new System.EventHandler(this.btn_Browse);
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(506, 412);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 3;
			this.btnOK.Text = "Open";
			this.btnOK.UseVisualStyleBackColor = true;
			// 
			// txtFilePath
			// 
			this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtFilePath.Location = new System.Drawing.Point(13, 426);
			this.txtFilePath.Name = "txtFilePath";
			this.txtFilePath.Size = new System.Drawing.Size(487, 20);
			this.txtFilePath.TabIndex = 4;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(506, 441);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// ddViewMode
			// 
			this.ddViewMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ddViewMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddViewMode.FormattingEnabled = true;
			this.ddViewMode.Items.AddRange(new object[] {
            "Large Icons",
            "Small Icons",
            "Details"});
			this.ddViewMode.Location = new System.Drawing.Point(494, 36);
			this.ddViewMode.MaxDropDownItems = 3;
			this.ddViewMode.Name = "ddViewMode";
			this.ddViewMode.Size = new System.Drawing.Size(87, 21);
			this.ddViewMode.TabIndex = 6;
			this.ddViewMode.SelectedValueChanged += new System.EventHandler(this.ddViewMode_SelectedValueChanged);
			// 
			// FileBrowser
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(593, 469);
			this.Controls.Add(this.ddViewMode);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.txtFilePath);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtFolder);
			this.Controls.Add(this.fileList);
			this.MinimumSize = new System.Drawing.Size(568, 490);
			this.Name = "FileBrowser";
			this.Text = "File Browser";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView fileList;
		private System.Windows.Forms.TextBox txtFolder;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox txtFilePath;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox ddViewMode;
		private System.Windows.Forms.ColumnHeader colFilename;
		private System.Windows.Forms.ColumnHeader colSize;
	}
}

