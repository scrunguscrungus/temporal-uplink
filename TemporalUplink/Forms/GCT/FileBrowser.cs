﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TemporalUplink.Forms
{
	public partial class FileBrowser : Form
	{
		public string FilePath
		{
			get
			{
				return txtFilePath.Text;
			}
		}

		private const string dirThumb = "./img/directory.png";
		private const string dirUpThumb = "./img/directory_up.png";

		private const int image_large_size = 128;
		private const int image_small_size = 64;

		private Ookii.Dialogs.VistaFolderBrowserDialog browseFolder;

		public FileBrowser( )
		{
			InitializeComponent( );
			browseFolder = new Ookii.Dialogs.VistaFolderBrowserDialog();
		}

		private void btn_Browse( object sender, EventArgs e )
		{
			//throw new NotImplementedException( );
			if ( browseFolder.ShowDialog() == DialogResult.OK )
			{
				txtFolder.Text = browseFolder.SelectedPath;
				NavigateToFolder(txtFolder.Text);
			}
		}

		private void txtFolder_Enter( object sender, KeyEventArgs e )
		{
			if ( e.KeyCode == Keys.Enter )
			{
#if !DEBUG
				try
				{
					NavigateToFolder( txtFolder.Text );
				}
				catch ( Exception ex)
				{
					MessageBox.Show( ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
				}
#else
				NavigateToFolder( txtFolder.Text );
#endif
				return;
			}
		}

		private void NavigateToFolder( string folder )
		{
			if ( !Directory.Exists(folder) )
			{
				MessageBox.Show( "The specified path could not be found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1 );
				return;
			}

			fileList.Items.Clear( );

			ImageList imgs = new ImageList();
			ImageList smallImgs = new ImageList( );

			imgs.ColorDepth = ColorDepth.Depth32Bit;
			smallImgs.ColorDepth = ColorDepth.Depth32Bit;

			imgs.ImageSize = new Size( image_large_size, image_large_size );
			smallImgs.ImageSize = new Size( image_small_size, image_small_size );

			fileList.LargeImageList = imgs;
			fileList.SmallImageList = smallImgs;

			imgs.Images.Add( "directory", Image.FromFile(dirThumb) );
			smallImgs.Images.Add( "directory", Image.FromFile(dirThumb) );

			imgs.Images.Add( "directory_up", Image.FromFile( dirUpThumb ) );
			smallImgs.Images.Add( "directory_up", Image.FromFile( dirUpThumb ) );

			if ( Path.GetPathRoot(folder) != folder)
			{
				ListViewItem newFile = new ListViewItem( "<Parent Directory>", "directory_up" );
				newFile.Tag = Directory.GetParent(folder).FullName;
				fileList.Items.Add( newFile );
			}

			//Directories come first, like Explorer
			foreach ( string dirPath in Directory.EnumerateDirectories(folder, "*", SearchOption.TopDirectoryOnly) )
			{
				ListViewItem newFile = new ListViewItem( Path.GetFileName( dirPath ), "directory" );
				newFile.Tag = dirPath;
				fileList.Items.Add( newFile );
			}

			foreach ( string filePath in Directory.EnumerateFiles(folder, "*.gct", SearchOption.TopDirectoryOnly))
			{
				Task.Run(() => { LoadImage(imgs, smallImgs, filePath); });

				ListViewItem newFile = new ListViewItem(Path.GetFileName(filePath), Path.GetFileName(filePath));
				newFile.Tag = filePath;
				newFile.SubItems.Add(new ListViewItem.ListViewSubItem(newFile, new FileInfo(filePath).Length.ToString() + " Bytes"));
				fileList.Items.Add(newFile);
			}
		}

		private void LoadImage(ImageList imgs, ImageList smallImgs, string filePath)
		{
			Image img;
			try
			{
				img = new TS3GCTLib.TS3GCT(filePath).bitmap;
			}
			catch
			{
				System.Diagnostics.Debug.WriteLine("Bad GCT: {0}", filePath);
				return;
			}
			int targetW = img.Width;
			int targetH = img.Height;

			float ratioW = 128.0f / targetW;
			float ratioH = 128.0f / targetH;

			float realRatio = Math.Min(ratioW, ratioH);

			targetH = (int)(targetH * realRatio);
			targetW = (int)(targetW * realRatio);

			Image newImg = new Bitmap(128, 128);

			using (Graphics GFX = Graphics.FromImage(newImg))
			{
				GFX.Clear(Color.Transparent);
				
				GFX.DrawImage(img, (128 / 2) - (targetW / 2), (128 / 2) - (targetH / 2), targetW, targetH);
				img.Dispose();
			}

			if (fileList.InvokeRequired)
			{
				fileList.Invoke((MethodInvoker)delegate ()
				{
					imgs.Images.Add(Path.GetFileName(filePath), newImg);
					smallImgs.Images.Add(Path.GetFileName(filePath), newImg);
				});
			}
			
		}

		private bool IsDirectory( string path )
		{
			return File.GetAttributes( path ).HasFlag( FileAttributes.Directory );
		}

		private void fileList_SelectedIndexChanged( object sender, EventArgs e )
		{
			if ( fileList.SelectedItems.Count < 1 || IsDirectory( (string)fileList.SelectedItems[ 0 ].Tag ))
				return;

			txtFilePath.Text = (string)fileList.SelectedItems[0].Tag;
		}

		private void fileList_ItemActivate( object sender, EventArgs e )
		{
			if ( fileList.SelectedItems.Count < 1 )
				return;

			if ( IsDirectory( (string)fileList.SelectedItems[ 0 ].Tag ) )
			{
				txtFolder.Text = (string)fileList.SelectedItems[ 0 ].Tag;
				NavigateToFolder( txtFolder.Text );
				return;
			}

			DialogResult = DialogResult.OK;
		}

		private void ddViewMode_SelectedValueChanged( object sender, EventArgs e )
		{
			switch (ddViewMode.SelectedIndex)
			{
				case 0:
					fileList.View = View.LargeIcon;
					break;
				case 1:
					fileList.View = View.SmallIcon;
					break;
				case 2:
					fileList.View = View.Details;
					break;
			}
		}
	}
}
