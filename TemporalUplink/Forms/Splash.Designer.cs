﻿namespace TemporalUplink.Forms
{
	partial class Splash
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( )
		{
			this.lblStartText = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblStartText
			// 
			this.lblStartText.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblStartText.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStartText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblStartText.Location = new System.Drawing.Point(-2, 132);
			this.lblStartText.Name = "lblStartText";
			this.lblStartText.Size = new System.Drawing.Size(512, 20);
			this.lblStartText.TabIndex = 0;
			this.lblStartText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblStartText.UseWaitCursor = true;
			// 
			// Splash
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.BackgroundImage = global::TemporalUplink.Properties.Resources.splash_temp;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(512, 152);
			this.Controls.Add(this.lblStartText);
			this.DoubleBuffered = true;
			this.ForeColor = System.Drawing.SystemColors.Control;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Splash";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Splash";
			this.TopMost = true;
			this.UseWaitCursor = true;
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lblStartText;
	}
}