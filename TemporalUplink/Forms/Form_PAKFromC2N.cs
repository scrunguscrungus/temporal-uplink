﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ookii.Dialogs;
using TS3PakLib;

namespace TemporalUplink.Forms
{

    public partial class Form_PAKFromC2N : Form
    {
        private VistaFolderBrowserDialog inputFolderDialog;

        public Form_PAKFromC2N( )
        {
            InitializeComponent( );
            inputFolderDialog = new VistaFolderBrowserDialog();
            TextBoxWriter writer = new TextBoxWriter( txtStatus );
            Console.SetOut( writer );
        }

        private void btnBrowseC2N_Click( object sender, EventArgs e )
        {
            if ( openC2NDialog.ShowDialog() == DialogResult.OK )
            {
                txtC2N.Text = openC2NDialog.FileName;
                /*

               
                
                */
            }
        }

        private void btnBrowseInput_Click( object sender, EventArgs e )
        {
            if ( inputFolderDialog.ShowDialog( ) == DialogResult.OK )
            {
                txtInput.Text = inputFolderDialog.SelectedPath;
            }
        }

        private void btnBrowseOutput_Click( object sender, EventArgs e )
        {
            if ( savePAKDialog.ShowDialog( ) == DialogResult.OK )
            {
                txtOutput.Text = savePAKDialog.FileName;
            }
        }

        private void btnBuild_Click( object sender, EventArgs e )
        {
            TS3Pak newPak = null;
            TS3C2N inputC2N;

            if ( !File.Exists(txtC2N.Text) || !Directory.Exists(txtInput.Text) )
            {
                MessageBox.Show( "A selected path was invalid.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error );
                return;
            }

            try
            {
                inputC2N = new TS3C2N(openC2NDialog.FileName);
            }
            catch (Exception exception)
            {
                MessageBox.Show("The C2N failed to load: " + exception.Message, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }


            try
            {
                Task makePak = Task.Run(() =>
                {
                    newPak = TS3Pak.CreatePAK(txtInput.Text, inputC2N, txtOutput.Text);
                });
                Task.WaitAll();
            }
            catch (Exception exception)
            {
                MessageBox.Show("PAK creation failed: " + exception.Message, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if ( newPak != null )
            {
                MessageBox.Show( "Operation complete", "Information", MessageBoxButtons.OK,
                    MessageBoxIcon.Information );
            }
        }


    }

    public class TextBoxWriter : TextWriter
    {
        private Control MyControl;

        delegate void SetTextCallback(string text);

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.MyControl.InvokeRequired)
            { 
                SetTextCallback d = new SetTextCallback(SetText);
                this.MyControl.Invoke(d, new object[] { text });
            }
            else
            {
                this.MyControl.Text = text;
            }
        }

        public TextBoxWriter( Control control )
        {
            MyControl = control;
        }

        public override void Write( char value )
        {
            SetText(  MyControl.Text + value );
        }

        public override void Write( string value )
        {
            SetText( MyControl.Text + value );
        }

        public override Encoding Encoding
        {
            get
            {
                return Encoding.Unicode;
            }
        }
    }

   
}
