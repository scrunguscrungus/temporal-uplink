﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3GCRLib;

namespace TemporalUplink.Forms
{
	public partial class Form_GCRInfo : Form
	{
		private TS3GCR currGCR;
		public Form_GCRInfo()
		{
			InitializeComponent();
		}


		private void TxtInfo_KeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = true;
			return;
		}

		private void BtnFindGCR_Click(object sender, EventArgs e)
		{
			txtInfo.Text = "Loading...";
			if (dlgFindGCR.ShowDialog() == DialogResult.OK)
			{
				try
				{
					currGCR = new TS3GCR(dlgFindGCR.FileName);
				}
				catch (Exception exception)
				{
					MessageBox.Show("The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
					return;
				}

				UpdateStats();

			}
		}
		private void UpdateStats()
		{
			if (currGCR == null)
				return;

			txtInfo.Text = "";

			txtInfo.Text += "GCR: " + Path.GetFileName(dlgFindGCR.FileName);
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Mesh count: " + currGCR.meshCount;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Textures: " + currGCR.textureCRCs.Count;
			txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "----------------";
			txtInfo.Text += Environment.NewLine;

			for ( int i=0;i< currGCR.textureCRCs.Count;i++)
			{
				byte[] tb = currGCR.textureCRCs[i];
				string crc = "";
				foreach ( byte b in tb )
				{
					crc += BitConverter.ToString(new byte[] { b });
				}
				txtInfo.Text += crc;
				txtInfo.Text += Environment.NewLine;
			}

		}
	}
}
