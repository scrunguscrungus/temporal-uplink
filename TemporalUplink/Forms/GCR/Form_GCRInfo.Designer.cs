﻿namespace TemporalUplink.Forms
{
	partial class Form_GCRInfo
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtInfo = new System.Windows.Forms.TextBox();
			this.btnFindGCR = new System.Windows.Forms.Button();
			this.lblGCRINfo = new System.Windows.Forms.Label();
			this.dlgFindGCR = new System.Windows.Forms.OpenFileDialog();
			this.lblPAK = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtInfo
			// 
			this.txtInfo.BackColor = System.Drawing.SystemColors.Window;
			this.txtInfo.Location = new System.Drawing.Point(15, 28);
			this.txtInfo.Multiline = true;
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.ReadOnly = true;
			this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtInfo.Size = new System.Drawing.Size(524, 185);
			this.txtInfo.TabIndex = 9;
			this.txtInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtInfo_KeyDown);
			// 
			// btnFindGCR
			// 
			this.btnFindGCR.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			this.btnFindGCR.Location = new System.Drawing.Point(500, 232);
			this.btnFindGCR.Name = "btnFindGCR";
			this.btnFindGCR.Size = new System.Drawing.Size(32, 23);
			this.btnFindGCR.TabIndex = 8;
			this.btnFindGCR.UseVisualStyleBackColor = true;
			this.btnFindGCR.Click += new System.EventHandler(this.BtnFindGCR_Click);
			// 
			// lblGCRINfo
			// 
			this.lblGCRINfo.AutoSize = true;
			this.lblGCRINfo.Location = new System.Drawing.Point(12, 9);
			this.lblGCRINfo.Name = "lblGCRINfo";
			this.lblGCRINfo.Size = new System.Drawing.Size(73, 13);
			this.lblGCRINfo.TabIndex = 7;
			this.lblGCRINfo.Text = "GCR File Info:";
			// 
			// dlgFindGCR
			// 
			this.dlgFindGCR.DefaultExt = "pak";
			this.dlgFindGCR.Filter = "TS3 GCR|*.gcr";
			// 
			// lblPAK
			// 
			this.lblPAK.AutoSize = true;
			this.lblPAK.Location = new System.Drawing.Point(497, 216);
			this.lblPAK.Name = "lblPAK";
			this.lblPAK.Size = new System.Drawing.Size(33, 13);
			this.lblPAK.TabIndex = 10;
			this.lblPAK.Text = "GCR:";
			// 
			// Form_GCRInfo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(572, 271);
			this.Controls.Add(this.txtInfo);
			this.Controls.Add(this.btnFindGCR);
			this.Controls.Add(this.lblGCRINfo);
			this.Controls.Add(this.lblPAK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form_GCRInfo";
			this.Text = "GCR Info";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtInfo;
		private System.Windows.Forms.Button btnFindGCR;
		private System.Windows.Forms.Label lblGCRINfo;
		private System.Windows.Forms.OpenFileDialog dlgFindGCR;
		private System.Windows.Forms.Label lblPAK;
	}
}