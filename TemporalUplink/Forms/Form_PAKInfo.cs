﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3PakLib;

namespace TemporalUplink.Forms
{
    public partial class Form_PAKInfo : Form
    {
        private TS3Pak currentPak = null;
        private TS3C2N currentC2N = null;

        public Form_PAKInfo( )
        {
            InitializeComponent( );
        }

        private void textBox2_KeyDown( object sender, KeyEventArgs e )
        {
            e.Handled = true;
            return;
        }

        private void button1_Click( object sender, EventArgs e )
        {
            txtInfo.Text = "Loading...";
            if ( dlgFindPAK.ShowDialog() == DialogResult.OK )
            {
                

                
                try
                {
                    currentPak = new TS3Pak(dlgFindPAK.FileName);
                }
                catch ( Exception exception )
                {
                    MessageBox.Show("The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                UpdateStats();;
                
            }
        }

        private void UpdateStats()
        {
            if (currentPak == null)
                return;

            txtInfo.Text = "";

            txtInfo.Text += "PAK: " + Path.GetFileName( dlgFindPAK.FileName );
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "File Count: " + currentPak.FileCount;
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "Info section start: " + "0x" + currentPak.info_offset.ToString( "X" );
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "Info section size: " + "0x" + currentPak.info_length.ToString( "X" );
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "Files:";
            txtInfo.Text += Environment.NewLine;


            foreach ( TS3Pak_FileInfo infoEntry in currentPak.FileInfo )
            {
                string fileName = "";
                if ( currentC2N != null )
                {
                    if ( currentC2N.TryGetIDPath((int)infoEntry.FileID, out fileName) )
                    {
                        txtInfo.Text += fileName;
                        txtInfo.Text += Environment.NewLine;
                        continue;
                    }
                }

                fileName = "0x" + infoEntry.FileID.ToString( "X" );
                txtInfo.Text += fileName;
                txtInfo.Text += Environment.NewLine;
            }
        }

        private void button2_Click( object sender, EventArgs e )
        {
            if ( dlgFindC2N.ShowDialog( ) == DialogResult.OK )
            {
                try
                {
                    currentC2N = new TS3C2N( dlgFindC2N.FileName );
                }
                catch ( Exception exception )
                {
                    MessageBox.Show( "The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error );
                    return;
                }

                UpdateStats( ); ;

            }
        }
    }
}
