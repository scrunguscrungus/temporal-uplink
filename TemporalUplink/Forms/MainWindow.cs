﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TemporalUplink.Forms;

namespace TemporalUplink
{
    public partial class MainWindow : Form
    {
		//PAK Tools
        private Form_PAKInfo pakInfoForm;
        private Form_PAKFromC2N pakFromC2NForm;
		private Form_PAKExtractAll pakExtractAll;
		private Form_PAKBrowser pakBrowse;


		//GCR Tools
		private Form_GCRInfo gcrInfoForm;

		//GCT Tools
		private Form_GCTInfo gctInfoForm;
		private Form_GCTView gctViewForm;

		//Misc Tools
		private Form_BSViewer bsViewForm;
		//private Form_MiscIdentify miscIdentifyForm;

        public MainWindow()
        {
            InitializeComponent();
        }

		////////////////////////////////////////////////////////////
		/// PAK tools
		////////////////////////////////////////////////////////////

		//
		// PAK Info
		//
        private void btnPAKInfo_Click( object sender, EventArgs e )
        {
            if ( pakInfoForm == null )
            {
                pakInfoForm = new Form_PAKInfo( );
                pakInfoForm.FormClosed += delegate
                {
                    pakInfoForm = null;
                };
            }
            pakInfoForm.Show( );
            pakInfoForm.BringToFront( );
        }
		//
		// Build PAK from C2N
		//
        private void btnBuildPAKC2N_Click( object sender, EventArgs e )
        {
            if ( pakFromC2NForm == null )
            {
                pakFromC2NForm = new Form_PAKFromC2N( );
                pakFromC2NForm.FormClosed += delegate
                {
                    pakFromC2NForm = null;
                };
            }
            pakFromC2NForm.Show( );
            pakFromC2NForm.BringToFront( );
        }
		//
		// Extract files
		//
		private void btnExtractOne_Click( object sender, EventArgs e )
		{
			if ( pakExtractAll == null )
			{
				pakExtractAll = new Form_PAKExtractAll( );
				pakExtractAll.FormClosed += delegate
				{
					pakExtractAll = null;
				};
			}
			pakExtractAll.Show( );
			pakExtractAll.BringToFront( );
		}

		private void btnBrowsePAK_Click(object sender, EventArgs e)
		{
			if (pakBrowse == null)
			{
				pakBrowse = new Form_PAKBrowser();
				pakBrowse.FormClosed += delegate
				{
					pakBrowse = null;
				};
			}
			pakBrowse.Show();
			pakBrowse.BringToFront();
		}

		//
		// GCR info
		//
		private void BtnGCRInfo_Click(object sender, EventArgs e)
		{
			if (gcrInfoForm == null)
			{
				gcrInfoForm = new Form_GCRInfo();
				gcrInfoForm.FormClosed += delegate
				{
					gcrInfoForm = null;
				};
			}
			gcrInfoForm.Show();
			gcrInfoForm.BringToFront();
		}

		//
		// GCT info
		//
		private void BtnGCTInfo_Click(object sender, EventArgs e)
		{
			if (gctInfoForm == null)
			{
				gctInfoForm = new Form_GCTInfo();
				gctInfoForm.FormClosed += delegate
				{
					gctInfoForm = null;
				};
			}
			gctInfoForm.Show();
			gctInfoForm.BringToFront();
		}
		//
		// GCT view
		//
		private void BtnGCTView_Click(object sender, EventArgs e)
		{
			if (gctViewForm == null)
			{
				gctViewForm = new Form_GCTView();
				gctViewForm.FormClosed += delegate
				{
					gctViewForm = null;
				};
			}
			gctViewForm.Show();
			gctViewForm.BringToFront();
		}

		////////////////////////////////////////////////////////////
		/// Misc
		////////////////////////////////////////////////////////////
		
		///BS files
		private void btnBSBrowser_Click(object sender, EventArgs e)
		{
			if (bsViewForm == null)
			{
				bsViewForm = new Form_BSViewer();
				bsViewForm.FormClosed += delegate
				{
					bsViewForm = null;
				};
			}
			bsViewForm.Show();
			bsViewForm.BringToFront();
		}

		/*private void btnIdentify_Click( object sender, EventArgs e )
		{
			if ( miscIdentifyForm == null )
			{
				miscIdentifyForm = new Form_MiscIdentify( );
				miscIdentifyForm.FormClosed += delegate
				{
					miscIdentifyForm = null;
				};
			}
			miscIdentifyForm.Show( );
			miscIdentifyForm.BringToFront( );
		}*/



		////////////////////////////////////////////////////////////
		/// Help
		////////////////////////////////////////////////////////////

		private void btnHelp_Click( object sender, EventArgs e )
		{
			System.Diagnostics.Process.Start( Properties.Resources.helpURL );
		}

	}
}
