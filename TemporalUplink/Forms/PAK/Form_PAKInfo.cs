﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3PakLib;
using TS3C2NLib;

namespace TemporalUplink.Forms
{
    public partial class Form_PAKInfo : Form
    {
        private TS3Pak currentPak = null;
        private TS3C2N currentC2N = null;

        public Form_PAKInfo( )
        {
            InitializeComponent( );
        }

        private void textBox2_KeyDown( object sender, KeyEventArgs e )
        {
            e.Handled = true;
            return;
        }

        private void button1_Click( object sender, EventArgs e )
        {
            txtInfo.Text = "Loading...";
            if ( dlgFindPAK.ShowDialog() == DialogResult.OK )
            {
                

                
                try
                {
					Task getPak = Task.Run( ( ) =>
					{
						try
						{
							currentPak = TS3Pak.ReadFromFile(dlgFindPAK.FileName);
						}
						catch ( Exception exception )
						{
							MessageBox.Show( "The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
								MessageBoxIcon.Error );
							return;
						}
					} );
					Task.WaitAll( );
                    
                }
                catch ( Exception exception )
                {
                    MessageBox.Show("The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                UpdateStats();
                
            }
        }

        private void UpdateStats()
        {
            if (currentPak == null)
                return;

            txtInfo.Text = "";

            txtInfo.Text += "PAK: " + Path.GetFileName( dlgFindPAK.FileName );
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "File Count: " + currentPak.FileCount;
            txtInfo.Text += Environment.NewLine;

			txtInfo.Text += "Info section start: " + "0x" + currentPak.FileDescOffset.ToString("X");
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "Info section size: " + "0x" + currentPak.FileDescLength.ToString( "X" );
            txtInfo.Text += Environment.NewLine;

            txtInfo.Text += "Files:";
            txtInfo.Text += Environment.NewLine;


            foreach ( TS3Pak_File infoEntry in currentPak.Files )
            {
                string fileName = "";
                if ( currentC2N != null )
                {
					try
					{
						fileName = currentC2N.FindEntry(infoEntry.FileCRC).Path;
					}
					catch
					{ fileName = string.Empty; } //???

                    if ( fileName != string.Empty )
                    {
                        txtInfo.Text += fileName;
						//txtInfo.Text += " (" + TS3FileIdentify.TS3Identify.GetNiceName( TS3FileIdentify.TS3Identify.Identify( currentPak.FileList[ infoEntry.FileIdx ] ) ) + ")";
                        txtInfo.Text += Environment.NewLine;
                        continue;
                    }
                }

				fileName = "0x" + infoEntry.FileCRC.ToString("X");
                txtInfo.Text += fileName;
				//txtInfo.Text += " (" + TS3FileIdentify.TS3Identify.GetNiceName( TS3FileIdentify.TS3Identify.Identify( currentPak.FileList[ infoEntry.FileIdx ] ) ) + ")";
                txtInfo.Text += Environment.NewLine;
            }
        }

        private void button2_Click( object sender, EventArgs e )
        {
            if ( dlgFindC2N.ShowDialog( ) == DialogResult.OK )
            {
                try
                {
					currentC2N = TS3C2N.ReadFromFile(dlgFindC2N.FileName);
                }
                catch ( Exception exception )
                {
                    MessageBox.Show( "The file could not be opened: " + exception.Message, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error );
                    return;
                }

                UpdateStats( ); ;

            }
        }
    }
}
