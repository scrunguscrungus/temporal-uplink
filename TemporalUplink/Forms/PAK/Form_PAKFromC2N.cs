﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ookii.Dialogs;
using TS3PakLib;
using TS3C2NLib;
using System.Diagnostics;

namespace TemporalUplink.Forms
{

    public partial class Form_PAKFromC2N : Form
    {
        private VistaFolderBrowserDialog inputFolderDialog;

        public Form_PAKFromC2N( )
        {
            InitializeComponent( );
            inputFolderDialog = new VistaFolderBrowserDialog();
        }

        private void btnBrowseC2N_Click( object sender, EventArgs e )
        {
            if ( openC2NDialog.ShowDialog() == DialogResult.OK )
            {
                txtC2N.Text = openC2NDialog.FileName;
            }
        }

        private void btnBrowseInput_Click( object sender, EventArgs e )
        {
            if ( inputFolderDialog.ShowDialog( ) == DialogResult.OK )
            {
                txtInput.Text = inputFolderDialog.SelectedPath;	
            }
        }

        private void btnBrowseOutput_Click( object sender, EventArgs e )
        {
            if ( savePAKDialog.ShowDialog( ) == DialogResult.OK )
            {
                txtOutput.Text = savePAKDialog.FileName;
            }
        }

        private void btnBuild_Click( object sender, EventArgs e )
        {
            TS3Pak newPak = null;
            //TS3C2N inputC2N;

			TUUtil.SetupDebugListener( txtStatus );

            if ( !File.Exists(txtC2N.Text) || !Directory.Exists(txtInput.Text) )
            {
                MessageBox.Show( "A selected path was invalid.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error );
                return;
            }

            try
            {
                //inputC2N = new TS3C2N(txtC2N.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show("The C2N failed to load: " + exception.Message, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }


            try
            {
                Task makePak = Task.Run(() =>
                {
                    //newPak = TS3Pak.CreatePAK(txtInput.Text, inputC2N, txtOutput.Text);
                });
                Task.WaitAll();
            }
            catch (Exception exception)
            {
                MessageBox.Show("PAK creation failed: " + exception.Message, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if ( newPak != null )
            {
                MessageBox.Show( "Operation complete", "Information", MessageBoxButtons.OK,
                    MessageBoxIcon.Information );
            }

        }


    }
   
}
