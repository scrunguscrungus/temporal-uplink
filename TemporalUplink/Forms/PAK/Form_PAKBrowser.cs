﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ookii.Dialogs;
using System.IO;
using TS3PakLib;
using TS3C2NLib;

namespace TemporalUplink.Forms
{
	public partial class Form_PAKBrowser : Form
	{
		MainMenu menu;

		OpenFileDialog openPAKDialog;
		OpenFileDialog openC2NDialog;
		SaveFileDialog extractDialogSingle;
		VistaFolderBrowserDialog extractDialogFolder;
		VistaFolderBrowserDialog buildPAKDialog;
		SaveFileDialog buildPAKSaveDialog;

		TS3Pak loadedPAK;
		TS3C2N loadedC2N;

		string pakPath;

		public Form_PAKBrowser()
		{
			InitializeComponent();

			menu = new MainMenu();

			MenuItem fileItem = new MenuItem("&File");
			MenuItem fileOpenItem = new MenuItem("&Open");
			MenuItem fileBuildPAK = new MenuItem("&Create PAK");

			fileItem.MenuItems.Add(fileOpenItem);
			fileItem.MenuItems.Add(fileBuildPAK);
			menu.MenuItems.Add(fileItem);
			fileOpenItem.Click += FileOpenItem_Click;
			fileBuildPAK.Click += FileBuildPAK_Click;
			Menu = menu;

			openPAKDialog = new OpenFileDialog();
			openPAKDialog.Filter = "PAK Files|*.pak";

			openC2NDialog = new OpenFileDialog();
			openC2NDialog.Filter = "C2N Files|*.c2n";

			extractDialogSingle = new SaveFileDialog();
			extractDialogSingle.Filter = "All Files|*.*";

			buildPAKSaveDialog = new SaveFileDialog();
			buildPAKSaveDialog.Filter = openPAKDialog.Filter;

			extractDialogFolder = new VistaFolderBrowserDialog();

			buildPAKDialog = new VistaFolderBrowserDialog();

			

			fileList.NodeMouseClick += FileList_NodeMouseClick;
			fileList.AfterSelect += FileList_AfterSelect;
		}

		private void FileBuildPAK_Click(object sender, EventArgs e)
		{
			if ( buildPAKDialog.ShowDialog() == DialogResult.OK)
			{
				string folderPath = buildPAKDialog.SelectedPath;

				TS3Pak resultPAK = TS3Pak.ConstructFromFolder(folderPath);
				TS3C2N resultC2N = TS3C2N.ConstructFromFolder(folderPath);

				if ( buildPAKSaveDialog.ShowDialog() == DialogResult.OK)
				{
					string outPath = buildPAKSaveDialog.FileName;

					string c2nName = Path.Combine(Path.GetDirectoryName(outPath), Path.GetFileNameWithoutExtension(outPath)) + ".c2n";

					resultPAK.WriteToFile(outPath);
					resultC2N.WriteToFile(c2nName);
				}
			}
		}

		private void FileList_AfterSelect(object sender, TreeViewEventArgs e)
		{
			//lblSelectedPath.Text = String.Format("Selected path: {0}{1}", Environment.NewLine, GetNodePath(fileList.SelectedNode));
		}

		private void FileList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if ( e.Button == MouseButtons.Right )
			{
				fileList.SelectedNode = e.Node;
			}
		}

		private void FileOpenItem_Click(object sender, EventArgs e)
		{
			fileList.Nodes.Clear();
			if ( openPAKDialog.ShowDialog() == DialogResult.OK )
			{
				pakPath = openPAKDialog.FileName;
				string pakName = Path.GetFileNameWithoutExtension(pakPath);
				string c2nPath = Path.Combine(Path.GetDirectoryName(pakPath), pakName + ".c2n");

				bool useC2N = true;

				if ( !File.Exists(c2nPath) )
				{
					if ( MessageBox.Show("A corresponding C2N was not found. Would you like to locate it manually?", "C2N not found", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes )
					{
						if ( openC2NDialog.ShowDialog() == DialogResult.OK )
						{
							c2nPath = openC2NDialog.FileName;
						}
						else
						{
							useC2N = false;
						}
					}
					else
					{
						useC2N = false;
					}
				}

				if ( !useC2N )
				{
					throw new NotImplementedException();
				}
				else
				{
					loadedC2N = TS3C2N.ReadFromFile(c2nPath);

					TreeNode rootNode = BuildTree();
					fileList.Nodes.Add(rootNode);
					SetupNodeContextMenus(rootNode);
				}

				loadedPAK = TS3Pak.ReadFromFile(pakPath);
				
			}
		}

		private bool IsDirectory( TreeNode node )
		{
			//We know that if a node has children, it must be a folder as it makes no sense
			//for a file to ever have children. 
			return node.Nodes.Count > 0;
		}

		private void SetupNodeContextMenus(TreeNode root)
		{
			ContextMenu cm = new ContextMenu();
			MenuItem extractItem = new MenuItem("&Extract");

			extractItem.Click += HandleExtract;

			cm.MenuItems.Add(extractItem);

			root.ContextMenu = cm;

			foreach ( TreeNode node in root.Nodes )
			{
				SetupNodeContextMenus(node);
			}
		}

		private void ExtractDir(string outPath, TreeNode root, TreeNode topLevel)
		{
			foreach( TreeNode node in root.Nodes)
			{
				if (IsDirectory(node))
				{
					ExtractDir(outPath, node, topLevel);
					continue;
				}

				string nodeFolder = Path.GetDirectoryName(GetNodePath(topLevel, node)).Replace("\\", "/").TrimStart('/');
				string fileName = node.Text;

				string outDir = Path.Combine(outPath, nodeFolder);
				if (!Directory.Exists(outDir))
					Directory.CreateDirectory(outDir);

				using (BinaryWriter bw = new BinaryWriter(File.Open(Path.Combine(outDir, fileName), FileMode.Create)))
				{
					bw.Write(loadedPAK.GetFile(GetNodePath(node).TrimStart('/')).FileData);
				}
			}
		}

		private void HandleExtract(object sender, EventArgs e)
		{
			if ( IsDirectory(fileList.SelectedNode) )
			{
				if (extractDialogFolder.ShowDialog() == DialogResult.OK)
				{
					string outPath = extractDialogFolder.SelectedPath.Replace("\\", "/");
					ExtractDir(outPath, fileList.SelectedNode, fileList.SelectedNode);
				}
			}
			else
			{
				extractDialogSingle.FileName = fileList.SelectedNode.Text;
				if ( extractDialogSingle.ShowDialog() == DialogResult.OK )
				{
					using (BinaryWriter bw = new BinaryWriter(File.Open(extractDialogSingle.FileName, FileMode.Create)))
					{
						bw.Write(loadedPAK.GetFile(GetNodePath(fileList.SelectedNode).TrimStart('/')).FileData);
					}
				}
			}
		}

		//https://stackoverflow.com/a/24861947/9307943
		private TreeNode BuildTree()
		{
			var rootNode = new TreeNode(Path.GetFileNameWithoutExtension(pakPath) + ".pak");
			foreach (var path in loadedC2N.Entries.Where(x => !string.IsNullOrEmpty(x.Path.Trim())))
			{
				var currentNode = rootNode;
				var pathItems = path.Path.Split('/');
				foreach (var item in pathItems)
				{
					var tmp = currentNode.Nodes.Cast<TreeNode>().Where(x => x.Text.Equals(item));
					currentNode = tmp.Count() > 0 ? tmp.Single() : currentNode.Nodes.Add(item);
				}
			}
			return rootNode;
		}

		//Get the path to a node, excluding root
		private string GetNodePath(TreeNode node)
		{
			if (node == null)
				return string.Empty;

			string basePath = node.FullPath;
			return basePath.Remove(basePath.IndexOf(fileList.Nodes[0].Text), fileList.Nodes[0].Text.Length);
		}
		//Working upwards, get the path from the bottom node to the top node, including the top node.
		private string GetNodePath(TreeNode topNode, TreeNode bottomNode)
		{
			if (topNode == null || bottomNode == null)
				return string.Empty;

			TreeNode targetNode;

			StringBuilder path = new StringBuilder();

			TreeNode currentNode = bottomNode;
			
			targetNode = topNode.Parent == null ? topNode : topNode.Parent;
			
			while ( currentNode != targetNode )
			{
				path.Insert(0, '/' + currentNode.Text);
				currentNode = currentNode.Parent;
			}

			return path.ToString();
		}


	}
}
