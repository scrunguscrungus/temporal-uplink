﻿using Ookii.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TS3PakLib;
using TS3C2NLib;

namespace TemporalUplink.Forms
{
	public partial class Form_PAKExtractAll : Form
	{
		private VistaFolderBrowserDialog outputFolderDialog;

		public Form_PAKExtractAll( )
		{
			InitializeComponent( );
			outputFolderDialog = new VistaFolderBrowserDialog( );
		}

		private void btnBrowseC2N_Click( object sender, EventArgs e )
		{
			if ( openC2NDialog.ShowDialog( ) == DialogResult.OK )
			{
				txtC2N.Text = openC2NDialog.FileName;
			}
		}

		private void btnBrowseInput_Click( object sender, EventArgs e )
		{
			if ( openPAKDialog.ShowDialog( ) == DialogResult.OK )
			{
				txtInput.Text = openPAKDialog.FileName;
			}
		}

		private void btnBrowseOutput_Click( object sender, EventArgs e )
		{
			if ( outputFolderDialog.ShowDialog( ) == DialogResult.OK )
			{
				txtOutput.Text = outputFolderDialog.SelectedPath;
			}
		}

		private void btnExtract_Click( object sender, EventArgs e )
		{
			TS3C2N c2nfile = null;
			//TS3Pak pakFile;

			TUUtil.SetupDebugListener( txtStatus );

			if ( !File.Exists( txtInput.Text ) )
			{
				MessageBox.Show( "A selected path was invalid.", "Error", MessageBoxButtons.OK,
					MessageBoxIcon.Error );
				return;
			}

			if ( txtC2N.Text != "" )
			{
				try
				{
					c2nfile = TS3C2N.ReadFromFile(txtC2N.Text);
				}
				catch ( Exception except )
				{
					MessageBox.Show( "Failed to open the C2N: " + except.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
					c2nfile = null;
				}
			}
			/*
			Task extractPak = Task.Run( ( ) =>
			{
				new TS3Pak( txtInput.Text ).ExtractAll( txtOutput.Text, c2nfile );
			} );
			Task.WaitAll( );
			*/
			
		}
	}
}
