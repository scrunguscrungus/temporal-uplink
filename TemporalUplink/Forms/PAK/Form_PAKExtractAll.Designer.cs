﻿namespace TemporalUplink.Forms
{
	partial class Form_PAKExtractAll
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( )
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_PAKExtractAll));
			this.openC2NDialog = new System.Windows.Forms.OpenFileDialog();
			this.openPAKDialog = new System.Windows.Forms.OpenFileDialog();
			this.pakTabs = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.btnBrowseOutput = new System.Windows.Forms.Button();
			this.btnBrowseInput = new System.Windows.Forms.Button();
			this.btnBrowseC2N = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtOutput = new System.Windows.Forms.TextBox();
			this.txtInput = new System.Windows.Forms.TextBox();
			this.txtC2N = new System.Windows.Forms.TextBox();
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.btnExtract = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.btnFindOutputFolder = new System.Windows.Forms.Button();
			this.btnFindInputFolder = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.lblFolder = new System.Windows.Forms.Label();
			this.txtOutputFolder = new System.Windows.Forms.TextBox();
			this.txtFolderInput = new System.Windows.Forms.TextBox();
			this.outFolder = new System.Windows.Forms.TextBox();
			this.btnExtractFolder = new System.Windows.Forms.Button();
			this.pakTabs.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// openC2NDialog
			// 
			this.openC2NDialog.DefaultExt = "c2n";
			resources.ApplyResources(this.openC2NDialog, "openC2NDialog");
			// 
			// openPAKDialog
			// 
			this.openPAKDialog.DefaultExt = "pak";
			resources.ApplyResources(this.openPAKDialog, "openPAKDialog");
			// 
			// pakTabs
			// 
			this.pakTabs.Controls.Add(this.tabPage1);
			this.pakTabs.Controls.Add(this.tabPage2);
			resources.ApplyResources(this.pakTabs, "pakTabs");
			this.pakTabs.Name = "pakTabs";
			this.pakTabs.SelectedIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.btnBrowseOutput);
			this.tabPage1.Controls.Add(this.btnBrowseInput);
			this.tabPage1.Controls.Add(this.btnBrowseC2N);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.txtOutput);
			this.tabPage1.Controls.Add(this.txtInput);
			this.tabPage1.Controls.Add(this.txtC2N);
			this.tabPage1.Controls.Add(this.txtStatus);
			this.tabPage1.Controls.Add(this.btnExtract);
			resources.ApplyResources(this.tabPage1, "tabPage1");
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// btnBrowseOutput
			// 
			this.btnBrowseOutput.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			resources.ApplyResources(this.btnBrowseOutput, "btnBrowseOutput");
			this.btnBrowseOutput.Name = "btnBrowseOutput";
			this.btnBrowseOutput.UseVisualStyleBackColor = true;
			this.btnBrowseOutput.Click += new System.EventHandler(this.btnBrowseOutput_Click);
			// 
			// btnBrowseInput
			// 
			this.btnBrowseInput.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			resources.ApplyResources(this.btnBrowseInput, "btnBrowseInput");
			this.btnBrowseInput.Name = "btnBrowseInput";
			this.btnBrowseInput.UseVisualStyleBackColor = true;
			this.btnBrowseInput.Click += new System.EventHandler(this.btnBrowseInput_Click);
			// 
			// btnBrowseC2N
			// 
			this.btnBrowseC2N.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			resources.ApplyResources(this.btnBrowseC2N, "btnBrowseC2N");
			this.btnBrowseC2N.Name = "btnBrowseC2N";
			this.btnBrowseC2N.UseVisualStyleBackColor = true;
			this.btnBrowseC2N.Click += new System.EventHandler(this.btnBrowseC2N_Click);
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// txtOutput
			// 
			resources.ApplyResources(this.txtOutput, "txtOutput");
			this.txtOutput.Name = "txtOutput";
			// 
			// txtInput
			// 
			resources.ApplyResources(this.txtInput, "txtInput");
			this.txtInput.Name = "txtInput";
			// 
			// txtC2N
			// 
			resources.ApplyResources(this.txtC2N, "txtC2N");
			this.txtC2N.Name = "txtC2N";
			// 
			// txtStatus
			// 
			this.txtStatus.BackColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.txtStatus, "txtStatus");
			this.txtStatus.ForeColor = System.Drawing.Color.LimeGreen;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.ReadOnly = true;
			// 
			// btnExtract
			// 
			resources.ApplyResources(this.btnExtract, "btnExtract");
			this.btnExtract.Name = "btnExtract";
			this.btnExtract.UseVisualStyleBackColor = true;
			this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.btnFindOutputFolder);
			this.tabPage2.Controls.Add(this.btnFindInputFolder);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Controls.Add(this.lblFolder);
			this.tabPage2.Controls.Add(this.txtOutputFolder);
			this.tabPage2.Controls.Add(this.txtFolderInput);
			this.tabPage2.Controls.Add(this.outFolder);
			this.tabPage2.Controls.Add(this.btnExtractFolder);
			resources.ApplyResources(this.tabPage2, "tabPage2");
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// btnFindOutputFolder
			// 
			this.btnFindOutputFolder.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			resources.ApplyResources(this.btnFindOutputFolder, "btnFindOutputFolder");
			this.btnFindOutputFolder.Name = "btnFindOutputFolder";
			this.btnFindOutputFolder.UseVisualStyleBackColor = true;
			// 
			// btnFindInputFolder
			// 
			this.btnFindInputFolder.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			resources.ApplyResources(this.btnFindInputFolder, "btnFindInputFolder");
			this.btnFindInputFolder.Name = "btnFindInputFolder";
			this.btnFindInputFolder.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// lblFolder
			// 
			resources.ApplyResources(this.lblFolder, "lblFolder");
			this.lblFolder.Name = "lblFolder";
			// 
			// txtOutputFolder
			// 
			resources.ApplyResources(this.txtOutputFolder, "txtOutputFolder");
			this.txtOutputFolder.Name = "txtOutputFolder";
			// 
			// txtFolderInput
			// 
			resources.ApplyResources(this.txtFolderInput, "txtFolderInput");
			this.txtFolderInput.Name = "txtFolderInput";
			// 
			// outFolder
			// 
			this.outFolder.BackColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.outFolder, "outFolder");
			this.outFolder.ForeColor = System.Drawing.Color.LimeGreen;
			this.outFolder.Name = "outFolder";
			this.outFolder.ReadOnly = true;
			// 
			// btnExtractFolder
			// 
			resources.ApplyResources(this.btnExtractFolder, "btnExtractFolder");
			this.btnExtractFolder.Name = "btnExtractFolder";
			this.btnExtractFolder.UseVisualStyleBackColor = true;
			// 
			// Form_PAKExtractAll
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.pakTabs);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form_PAKExtractAll";
			this.pakTabs.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.OpenFileDialog openC2NDialog;
		private System.Windows.Forms.OpenFileDialog openPAKDialog;
		private System.Windows.Forms.TabControl pakTabs;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Button btnBrowseOutput;
		private System.Windows.Forms.Button btnBrowseInput;
		private System.Windows.Forms.Button btnBrowseC2N;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtOutput;
		private System.Windows.Forms.TextBox txtInput;
		private System.Windows.Forms.TextBox txtC2N;
		private System.Windows.Forms.TextBox txtStatus;
		private System.Windows.Forms.Button btnExtract;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button btnFindOutputFolder;
		private System.Windows.Forms.Button btnFindInputFolder;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblFolder;
		private System.Windows.Forms.TextBox txtOutputFolder;
		private System.Windows.Forms.TextBox txtFolderInput;
		private System.Windows.Forms.TextBox outFolder;
		private System.Windows.Forms.Button btnExtractFolder;
	}
}