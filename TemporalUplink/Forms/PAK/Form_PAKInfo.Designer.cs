﻿namespace TemporalUplink.Forms
{
    partial class Form_PAKInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
			this.lblPAKFile = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.txtInfo = new System.Windows.Forms.TextBox();
			this.dlgFindPAK = new System.Windows.Forms.OpenFileDialog();
			this.lblPAK = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.lblC2N = new System.Windows.Forms.Label();
			this.dlgFindC2N = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// lblPAKFile
			// 
			this.lblPAKFile.AutoSize = true;
			this.lblPAKFile.Location = new System.Drawing.Point(13, 13);
			this.lblPAKFile.Name = "lblPAKFile";
			this.lblPAKFile.Size = new System.Drawing.Size(71, 13);
			this.lblPAKFile.TabIndex = 0;
			this.lblPAKFile.Text = "PAK File Info:";
			// 
			// button1
			// 
			this.button1.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			this.button1.Location = new System.Drawing.Point(368, 239);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(32, 23);
			this.button1.TabIndex = 2;
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtInfo
			// 
			this.txtInfo.BackColor = System.Drawing.SystemColors.Window;
			this.txtInfo.Location = new System.Drawing.Point(16, 32);
			this.txtInfo.Multiline = true;
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.ReadOnly = true;
			this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtInfo.Size = new System.Drawing.Size(524, 185);
			this.txtInfo.TabIndex = 3;
			// 
			// dlgFindPAK
			// 
			this.dlgFindPAK.DefaultExt = "pak";
			this.dlgFindPAK.Filter = "TS3 PAK|*.pak";
			// 
			// lblPAK
			// 
			this.lblPAK.AutoSize = true;
			this.lblPAK.Location = new System.Drawing.Point(368, 220);
			this.lblPAK.Name = "lblPAK";
			this.lblPAK.Size = new System.Drawing.Size(31, 13);
			this.lblPAK.TabIndex = 4;
			this.lblPAK.Text = "PAK:";
			// 
			// button2
			// 
			this.button2.Image = global::TemporalUplink.Properties.Resources.img_BrowseFile;
			this.button2.Location = new System.Drawing.Point(427, 239);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(32, 23);
			this.button2.TabIndex = 5;
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// lblC2N
			// 
			this.lblC2N.AutoSize = true;
			this.lblC2N.Location = new System.Drawing.Point(424, 220);
			this.lblC2N.Name = "lblC2N";
			this.lblC2N.Size = new System.Drawing.Size(31, 13);
			this.lblC2N.TabIndex = 6;
			this.lblC2N.Text = "C2N:";
			// 
			// dlgFindC2N
			// 
			this.dlgFindC2N.DefaultExt = "pak";
			this.dlgFindC2N.Filter = "TS3 C2N|*.C2N";
			// 
			// Form_PAKInfo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(552, 272);
			this.Controls.Add(this.lblC2N);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.lblPAK);
			this.Controls.Add(this.txtInfo);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lblPAKFile);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form_PAKInfo";
			this.Text = "PAK Info";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPAKFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.OpenFileDialog dlgFindPAK;
        private System.Windows.Forms.Label lblPAK;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblC2N;
        private System.Windows.Forms.OpenFileDialog dlgFindC2N;
    }
}