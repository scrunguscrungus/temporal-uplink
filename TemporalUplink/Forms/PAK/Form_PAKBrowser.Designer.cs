﻿namespace TemporalUplink.Forms
{
	partial class Form_PAKBrowser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fileList = new System.Windows.Forms.TreeView();
			this.SuspendLayout();
			// 
			// fileList
			// 
			this.fileList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fileList.BackColor = System.Drawing.Color.Gray;
			this.fileList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.fileList.ForeColor = System.Drawing.SystemColors.Window;
			this.fileList.Location = new System.Drawing.Point(231, 0);
			this.fileList.Name = "fileList";
			this.fileList.PathSeparator = "/";
			this.fileList.Size = new System.Drawing.Size(380, 553);
			this.fileList.TabIndex = 0;
			// 
			// Form_PAKBrowser
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.ClientSize = new System.Drawing.Size(611, 553);
			this.Controls.Add(this.fileList);
			this.Name = "Form_PAKBrowser";
			this.Text = "PAK Browser";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TreeView fileList;
	}
}