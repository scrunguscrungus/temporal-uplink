﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TS3FileIdentify
{
	/// <summary>
	/// Static methods for identifying an unknown file
	/// </summary>
    public static class TS3Identify
    {
		/// <summary>
		/// Filetypes
		/// </summary>
		public enum TS3FileType
		{
			Unknown = 0,
			PAK = 1,
			GCR = 2,
			GCT = 3,
			ANIM = 4,
			PAD = 5,
			SOUNDPAD = 6,
			CSINFO = 7
		}

		/// <summary>
		/// Gets the file extension for a particular filetype.
		/// </summary>
		static Dictionary<TS3FileType, string> typeExtension = new Dictionary<TS3FileType, string>
		{
			{ TS3FileType.Unknown, "" },
			{ TS3FileType.PAK, ".PAK"},
			{ TS3FileType.GCR, ".GCR"},
			{ TS3FileType.GCT, ".GCT"},
			{ TS3FileType.ANIM, ".ANM"},
			{ TS3FileType.PAD, ".PAD"},
			{ TS3FileType.SOUNDPAD, ".s"},
			{ TS3FileType.CSINFO, ".txt"}
		};
		/// <summary>
		/// Gets a nice name for a filetype
		/// </summary>
		static Dictionary<TS3FileType, string> typeNiceName = new Dictionary<TS3FileType, string>
		{
			{ TS3FileType.Unknown, "Unknown" },
			{ TS3FileType.PAK, "TS3 PAK Archive"},
			{ TS3FileType.GCR, "TS3 GameCube Model"},
			{ TS3FileType.GCT, "TS3 GameCube Texture"},
			{ TS3FileType.ANIM, "TS3 Animation"},
			{ TS3FileType.PAD, "TS3 Level Pad Data"},
			{ TS3FileType.SOUNDPAD, "TS3 Level Sound Pad Data"},
			{ TS3FileType.CSINFO, "Text File (Cutscene Info)"}
		};

		/// <summary>
		/// Magic numbers for types
		/// </summary>
		static Dictionary<byte[], TS3FileType> typeMagic = new Dictionary<byte[], TS3FileType>
		{
			{ new byte[] {0x50, 0x35, 0x43, 0x4B}, TS3FileType.PAK }, //P5CK
		    //{ new byte[] {0x00, 0x00, 0x00, 0x0c}, TS3FileType.GCR  },
			{ new byte[] { 0x53, 0x54, 0x52, 0x45, 0x41, 0x4d }, TS3FileType.CSINFO } //STREAM
		};

		public static string GetNiceName( TS3FileType type )
		{
			string niceName = "Unknown";

			typeNiceName.TryGetValue( type, out niceName );

			return niceName;
		}

		public static TS3FileType Identify( string path )
		{
			if ( !File.Exists(path) )
			{
				throw new IOException("The specified file does not exist.");
			}

			byte[] topRow;
			TS3FileType type;

			using ( BinaryReader br = new BinaryReader( File.OpenRead(path) ) )
			{
				topRow = br.ReadBytes(16);
			}
			
			foreach ( byte[] magic in typeMagic.Keys)
			{
				if ( Enumerable.SequenceEqual(topRow.Take( magic.Length ).ToArray(), magic) )
				{
					if ( typeMagic.TryGetValue(magic, out type) )
					{
						return type;
					}
				}
			}

			return TS3FileType.Unknown;
		}
		public static TS3FileType Identify( BinaryReader br )
		{
			byte[ ] topRow;
			TS3FileType type;

			topRow = br.ReadBytes( 16 );

			foreach ( byte[ ] magic in typeMagic.Keys )
			{
				if ( Enumerable.SequenceEqual( topRow.Take( magic.Length ).ToArray( ), magic ) )
				{
					if ( typeMagic.TryGetValue( magic, out type ) )
					{
						return type;
					}
				}
			}

			return TS3FileType.Unknown;
		}
		public static TS3FileType Identify( byte[] bytes )
		{
			using ( BinaryReader br = new BinaryReader(new MemoryStream(bytes)))
			{
				return Identify( br );
			}
		}
    }
}
