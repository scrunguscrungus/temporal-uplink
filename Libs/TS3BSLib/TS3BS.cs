﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TS3ExtensionsLib;

namespace TS3BSLib
{
    public class TS3BS
	{
		#region Private Fields
		private List<string> entries = new List<string>( );
		#endregion

		#region Public Properties
		public List<string> Entries
		{
			get { return entries; }
			set { entries = value;  }
		}
		#endregion

		#region Reading
		public static TS3BS ReadFromFile( string filepath )
		{
			using (BinaryReader br = new BinaryReader( File.OpenRead(filepath), Encoding.Default ))
			{
				TS3BS readBS = new TS3BS();

				//Dummy value
				int stringStart = int.MaxValue;

				while ( br.BaseStream.Position < stringStart )
				{
					string newEntry;

					int pos = br.ReadInt32(TS3_Endian.BIG_ENDIAN);

					if ( readBS.entries.Count < 1 )
						stringStart = pos;

					long oldPos = br.BaseStream.Position;

					br.BaseStream.Seek( pos, SeekOrigin.Begin );
					newEntry = br.ReadNTString();
					br.BaseStream.Seek( oldPos, SeekOrigin.Begin );

					readBS.entries.Add( newEntry );
				}

				return readBS;
			}
		}

		public static TS3BS ConstructFromTXT(string filepath)
		{
			TS3BS constructedBS = new TS3BS();

			string[] inLines = File.ReadAllLines(filepath);

			foreach ( string entry in inLines )
			{
				constructedBS.entries.Add(entry);
			}

			return constructedBS;
		}
		#endregion

		#region Writing
		public void WriteToFile( string outPath )
		{
			using ( BinaryWriter bw = new BinaryWriter( File.Open(outPath, FileMode.Create) ))
			{
				Dictionary<string, int> cache = new Dictionary<string, int>();
				int stringOffset = entries.Count * 4;

				foreach ( string entry in entries )
				{
					string outString = entry.Replace("\\n", "\n");
					int outPosition = 0;

					if (outString.Length < 1 || outString.Last() != '\0')
						outString += '\0';

					if ( !cache.TryGetValue(outString, out outPosition) )
					{

						outPosition = stringOffset;

						stringOffset += outString.Length;

						cache.Add(outString, outPosition);
					}

					bw.Write(outPosition, TS3_Endian.BIG_ENDIAN);

					long oldPos = bw.BaseStream.Position;

					bw.Seek(outPosition, SeekOrigin.Begin);

					foreach ( char chr in outString)
					{
						bw.Write(Encoding.Default.GetBytes(new char[] { chr }));
					}

					bw.Seek((int)oldPos, SeekOrigin.Begin);
				}
			}
		}
		#endregion
	}
}
