﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TS3ExtensionsLib;

namespace TS3C2NLib
{
	public struct C2NEntry
	{
		public uint CRC;
		public string Path;
	}

    public class TS3C2N
	{
		#region Private Fields
		private List<C2NEntry> entries = new List<C2NEntry>( );
		private static string seperator = "  ";
		#endregion

		#region Public Properties
		public List<C2NEntry> Entries
		{
			get { return entries; }
		}
		#endregion


		#region Reading
		public C2NEntry FindEntry( uint crc )
		{
			try
			{
				return ( from entry in entries
						 where entry.CRC == crc
						 select entry ).Single( );
			}
			catch ( InvalidOperationException )
			{
				throw new FileNotFoundException( "The specified CRC was not found in the C2N" );
			}
		}
		public C2NEntry FindEntry( string path )
		{
			try
			{
				return ( from entry in entries
						 where entry.Path == path
						 select entry ).Single( );
			}
			catch ( InvalidOperationException )
			{
				throw new FileNotFoundException( "The specified path was not found in the C2N" );
			}
		}

		//THINK ABOUT: Should we make this accept a stream instead of a filepath?
		//			   Basically offloading the responsibility of making sure the path is
		//			   valid, etc. to whatever is using the lib.
		public static TS3C2N ReadFromFile( string filePath )
		{
			TS3C2N newC2N = new TS3C2N( );

			string[ ] lines = File.ReadAllLines( filePath );

			foreach ( string line in lines )
			{
				string[ ] split = line.Split( new string[ ] { seperator }, StringSplitOptions.None );

				C2NEntry entry = new C2NEntry( );

				entry.CRC = Convert.ToUInt32( split[ 0 ], 16 );
				entry.Path = split[ 1 ];

				newC2N.entries.Add( entry );
			}

			return newC2N;
		}

		public static TS3C2N ConstructFromFolder( string folderPath )
		{
			TS3C2N newC2N = new TS3C2N( );

			var dir = Directory.EnumerateFiles( folderPath, "*.*", SearchOption.AllDirectories );

			Crc32 crc = new Crc32( );
			foreach ( string filePath in dir )
			{
				string relativePath = TS3Extensions.GetRelativePath( filePath, folderPath );

				C2NEntry entry = new C2NEntry( );

				entry.CRC = crc.Get( Encoding.ASCII.GetBytes( relativePath ) );
				entry.Path = relativePath;

				newC2N.entries.Add( entry );
			}

			return newC2N;
		}
		#endregion

		#region Writing
		public void WriteToFile( string outPath )
		{
			using ( StreamWriter sw = new StreamWriter(File.Open(outPath, FileMode.Create)))
			{
				foreach ( C2NEntry entry in entries )
				{
					sw.WriteLine( String.Format( "0x{0}{1}{2}", entry.CRC.ToString( "x" ), seperator, entry.Path ) );
				}
			}
		}
		#endregion
	}
}
