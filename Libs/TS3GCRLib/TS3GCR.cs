﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TS3ExtensionsLib;

namespace TS3GCRLib
{
	/////
	/// TS3GCRLib - Library for reading/writing TS3 models
	/////

	public class TS3GCR
	{
		public int textureDataOffset;
		public int meshCountOffset;
		public int meshCount;

		public List<byte[]> textureCRCs = new List<byte[]>();
		private byte[] textureChunkEnd = { 0xFF, 0xFF, 0xFF, 0xFF };

		//Our reader
		private BinaryReader GCRFS;

		public TS3GCR(string path)
		{
			GCRFS = new BinaryReader(File.OpenRead(path));

			GCRFS.BaseStream.Seek(0, SeekOrigin.Begin);

			textureDataOffset = GCRFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			meshCountOffset = GCRFS.ReadInt32(TS3_Endian.BIG_ENDIAN);

			GCRFS.BaseStream.Seek(textureDataOffset, SeekOrigin.Begin);

			byte[] currID = new byte[4];
			while (!Enumerable.SequenceEqual(currID, textureChunkEnd))
			{
				currID = GCRFS.ReadBytes(4);
				if (!Enumerable.SequenceEqual(currID, textureChunkEnd))
				{
					textureCRCs.Add(currID);
				}
				GCRFS.BaseStream.Seek(12, SeekOrigin.Current);
			}

			GCRFS.BaseStream.Seek(meshCountOffset, SeekOrigin.Begin);

			meshCount = GCRFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
		}
	}
}
