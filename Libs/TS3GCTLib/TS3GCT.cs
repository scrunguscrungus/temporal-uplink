﻿using System;
using System.Collections.Generic;
using System.IO;
using TS3ExtensionsLib;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TS3GCTLib
{
	/////
	/// TS3GCTLib - Library for reading/writing TS3 textures
	/////
	

	public class TS3GCT
	{
		[Flags]
		public enum GCT_Flag
		{
			FLAG_NONE = 0,
			FLAG_1 = (1 << 0),
			FLAG_2 = (1 << 1),
			FLAG_3 = (1 << 2),
			FLAG_4 = (1 << 3),
			FLAG_5 = (1 << 4),
			FLAG_6 = (1 << 5),
			FLAG_7 = (1 << 6),
			FLAG_8 = (1 << 7),
			FLAG_9 = (1 << 8),
			FLAG_10 = (1 << 9),
			FLAG_11 = (1 << 10),
			FLAG_12 = (1 << 11),
			FLAG_13 = (1 << 12),
			FLAG_14 = (1 << 13),
			FLAG_15 = (1 << 14),
			FLAG_16 = (1 << 15),
			FLAG_17 = (1 << 16),
			FLAG_18 = (1 << 17),
			FLAG_19 = (1 << 18),
			FLAG_20 = (1 << 19),
			FLAG_21 = (1 << 20),
			FLAG_22 = (1 << 21),
			FLAG_23 = (1 << 22),
			FLAG_24 = (1 << 23),
			FLAG_25 = (1 << 24),
			FLAG_26 = (1 << 25),
			FLAG_27 = (1 << 26),
			FLAG_28 = (1 << 27),
			FLAG_29 = (1 << 28),
			FLAG_30 = (1 << 29),
			FLAG_31 = (1 << 30),
			FLAG_32 = (1 << 31)
		}
		string filePath;

		public int width1;
		public int height1;
		public int width2;
		public int height2;
		public GCT_Flag texFlags;
		public int unknown;

		const byte textureDataStartOffset = 0x20;

		long flagOffset;

		//Color[][] blocks_DXT1;
		public Color[][] blocks_CMPR;
		public Bitmap bitmap;

		//Our reader
		private BinaryReader GCTFS;

		public TS3GCT(string path)
		{
			filePath = path;

			GCTFS = new BinaryReader(File.OpenRead(path));

			GCTFS.BaseStream.Seek(0, SeekOrigin.Begin);

			width1 = GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			height1 = GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			width2 = GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			height2 = GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			flagOffset = GCTFS.BaseStream.Position;
			texFlags = (GCT_Flag)GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);
			unknown = GCTFS.ReadInt32(TS3_Endian.BIG_ENDIAN);

			GCTFS.BaseStream.Seek(textureDataStartOffset, SeekOrigin.Begin);

			blocks_CMPR = DXT.ReadCMPR(GCTFS, width2, height2);

			/*
			 * THE FOLLOWING CODE IS
			 * COPYRIGHT (C) JASON HARLEY
			 * FROM GAMETOOLS:
			 * https://bitbucket.org/jas2o/gametools/src/master/
			*/
			bitmap = new Bitmap(width2, height2, System.Drawing.Imaging.PixelFormat.Format32bppRgb);


			int bi = 0;
			for (int y = 0; y < height2 / 4; y++)
			{
				for (int x = 0; x < width2 / 4; x++)
				{
					for (int i = 0; i < 16; i++)
					{
						int row = i / 4;
						int col = i - (row * 4);
						bitmap.SetPixel(x * 4 + col, y * 4 + row, blocks_CMPR[bi][i]);
					}
					bi++;
				}
			}

			GCTFS.Close();
		}

		/*This will be reworked when we have proper conversion of formats to GCT*/
		public void SaveFlags(int newFlags)
		{
			using (BinaryWriter bw = new BinaryWriter(File.Open(filePath, FileMode.Open)))
			{
				bw.Seek((int)flagOffset, SeekOrigin.Begin);
				bw.Write(newFlags, TS3_Endian.BIG_ENDIAN);
				texFlags = (GCT_Flag)newFlags;
			}
		}
	}
}
