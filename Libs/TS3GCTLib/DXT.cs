﻿/*
 * DXT AND CMPR READING CODE (with the exception of revamped reordering code by Bekoha) IS
 * COPYRIGHT (C) JASON HARLEY
 * FROM GAMETOOLS:
 * https://bitbucket.org/jas2o/gametools/src/master/
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using TS3ExtensionsLib;

namespace TS3GCTLib
{
	public static class DXT
	{
		public static Color[][] ReadCMPR(BinaryReader fs, int width, int height)
		{
			int total = width * height;
			Color[][] blocks = ReadDXT1(fs, width, height);

			//Fixed and revamped CMPR reordering code
			//Huge thank you to Bekoha for figuring this out
			Color[][] reorder = new Color[total / 16][];
			int cell = 0;
			int blockscount = width / 4;
			int offset = 0;
			int head = 0;
			for (int rowshead = 0; rowshead < height / 4; rowshead += 2)
			{
				for (; head < blockscount * (rowshead + 1); head += 2)
				{
					reorder[cell + offset] = blocks[head];
					reorder[cell + 1 + offset] = blocks[head + 1];
					reorder[cell + offset + blockscount / 2] = blocks[head + blockscount];
					reorder[cell + 1 + offset + blockscount / 2] = blocks[head + blockscount + 1];
					if (head % 4 != 0)
					{
						offset = 0;
						cell += 2;
					}
					else
					{
						offset = blockscount;
					}
				}
				head = cell = blockscount * (rowshead + 2);
			}
			/*
			for (int y = 0; y < rows; y += 4)
			{
				for (int x = 0; x < cols; x += 4)
				{
					reorder[i++] = blocks[y * rows / 4 + x + 0];
					reorder[i++] = blocks[y * rows / 4 + x + 1];
				}
				for (int x = 0; x < cols; x += 4)
				{
					reorder[i++] = blocks[y * rows / 4 + x + 2];
					reorder[i++] = blocks[y * rows / 4 + x + 3];
				}
			}*/

			return reorder;
		}

		public static Color[][] ReadDXT1(BinaryReader fs, int width, int height)
		{
			int total = width * height;
			Color[][] blocks = new Color[total / 16][];

			for (int i = 0; i < total / 16; i++)
			{

				byte[] raw = fs.ReadBytes(4, TS3_Endian.BIG_ENDIAN);

				ushort c0, c1;
				c0 = (ushort)(raw[2] | raw[3] << 8);
				c1 = (ushort)(raw[0] | raw[1] << 8);
				// Color 0 RGB
				byte r0 = (byte)((c0 >> 11) << 3 | 0x04);
				byte g0 = (byte)((c0 & 0x07E0) >> 3 | 0x02);
				byte b0 = (byte)((c0 & 0x001F) << 3 | 0x04);
				// Color 1 RBG
				byte r1 = (byte)((c1 >> 11) << 3);
				byte g1 = (byte)((c1 & 0x07E0) >> 3);
				byte b1 = (byte)((c1 & 0x001F) << 3);
				//--

				Color[] cx = new Color[] {
						Color.FromArgb(Byte.MaxValue, r0, g0, b0),
						Color.FromArgb(Byte.MaxValue, r1, g1, b1),
						(c0 > c1) ? Color.FromArgb(byte.MaxValue, (2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3) : Color.FromArgb(byte.MaxValue, (2 * r1 + r0) / 3, (2 * g1 + g0) / 3, (2 * b1 + b0) / 3),
						(c0 <= c1) ? Color.FromArgb(byte.MaxValue, (2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3) : Color.FromArgb(byte.MaxValue, (2 * r1 + r0) / 3, (2 * g1 + g0) / 3, (2 * b1 + b0) / 3)
					};

				uint pixels = fs.ReadUInt32(TS3_Endian.BIG_ENDIAN);

				int p15 = (int)((pixels & 0xC0000000) >> 30);
				int p14 = (int)((pixels & 0x30000000) >> 28);
				int p13 = (int)((pixels & 0xC000000) >> 26);
				int p12 = (int)((pixels & 0x3000000) >> 24);
				int p11 = (int)((pixels & 0xC00000) >> 22);
				int p10 = (int)((pixels & 0x300000) >> 20);
				int p9 = (int)((pixels & 0xC0000) >> 18);
				int p8 = (int)((pixels & 0x30000) >> 16);
				int p7 = (int)((pixels & 0xC000) >> 14);
				int p6 = (int)((pixels & 0x3000) >> 12);
				int p5 = (int)((pixels & 0xC00) >> 10);
				int p4 = (int)((pixels & 0x300) >> 8);
				int p3 = (int)((pixels & 0xC0) >> 6);
				int p2 = (int)((pixels & 0x30) >> 4);
				int p1 = (int)((pixels & 0xC) >> 2);
				int p0 = (int)((pixels & 0x3));

				blocks[i] = new Color[] {
						cx[p15], cx[p14], cx[p13], cx[p12],
						cx[p11], cx[p10], cx[p9], cx[p8],
						cx[p7], cx[p6], cx[p5], cx[p4],
						cx[p3], cx[p2], cx[p1], cx[p0]
				};
			}

			return blocks;
		}
	}
}
