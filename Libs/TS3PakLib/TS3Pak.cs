﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using TS3ExtensionsLib;


/////
/// TS3PakLib - Library for reading/writing TS3 PAKfiles
/// Adapted from TSFP_Unpak by Peter Hall (A tremendous thank you to him for all his help!)
/// https://github.com/tastymorsel/tsfp_unpak
/////

namespace TS3PakLib
{
	public struct TS3Pak_File
	{
		//Unique identifier, CRC32 of filepath
		public uint FileCRC;

		//Position of this file's data in the PAK.
		public int FilePos;

		//Original length of this file.
		public int FileLength;

		//Compressed length. If this is nonzero, this will be the file's length in the PAK and it needs to be decompressed. Otherwise, use FileLength.
		//Compression of files when creating PAKs is currently unsupported (game doesn't care)
		public int FileCompressedLength;
		
		public byte[] FileData;

	}

	// 2/7/19
	// Refactor
	// Changes:
	//
	// Complete rewrite of entire class
	//
	// Per new design principles, this lib no longer supports extracting files itself
	// - Users of the library can retrieve files by passing a CRC32 or nice path to GetFiles() to get a TS3Pak_File containing info
	//   about the file, including a byte array of the file's data. It is up to them to handle writing this data (see Temporal
	//	 Uplink GUI. Form_PAKBrowser, for example implementation).
	//
	// TS3Pak_FileInfo is now TS3Pak_File
	//
	// Main PAK class no longer stores massive list of byte arrays for file data. File defs (TS3Pak_File) now store their own data.
	//
	// Seperated C2N files into their own library. While PAKs and C2Ns have a close relationship, they are 2 distinct formats which
	// don't directly interact and can exist without each other. As a result, applications should be responsible for their own
	// handling when it comes to using the two formats together.

    public class TS3Pak
    {
#region Private Fields
		private int info_offset;
		private int info_length;

		private List<TS3Pak_File> file_list = new List<TS3Pak_File>();

		private static byte[ ] magicNumber = new byte[ 4 ] { 0x50, 0x35, 0x43, 0x4B };

		private static int ByteAlignment = 32;

		/*							   Magic	Info offset		 Info length	 Unused?     */
		private static int headerLen = 4	+	sizeof( int ) + sizeof( int ) + sizeof( int );
#endregion

#region Public Properties
		public long FileDescOffset
		{
			get { return info_offset; }
		}
		public int FileDescLength
		{
			get { return info_length; }
		}
		public List<TS3Pak_File> Files
		{
			get { return file_list; }
		}
		
		public int FileCount
		{
			get { return info_length / 16;  }
		}
#endregion

#region Reading
		public TS3Pak_File GetFile(uint crc)
		{
			try
			{
				return ( from file in file_list
						 where file.FileCRC == crc
						 select file ).Single( );
			}
			catch (InvalidOperationException)
			{
				throw new FileNotFoundException( "The specified file was not found in the PAK." );
			}
		}
		public TS3Pak_File GetFile(string nicePath)
		{
			return GetFile( new Crc32( ).Get( Encoding.ASCII.GetBytes(nicePath) ) );
		}

		//THINK ABOUT: Should we make this accept a stream instead of a filepath??
		//			   Basically offloading the responsibility of making sure the path is
		//			   valid, etc. to whatever is using the lib.
		//			   Should we support both??

		//It's [current year] and CA2202 still exists for some reason.
		public static TS3Pak ReadFromFile( string filePath )
		{
			using ( BinaryReader reader = new BinaryReader( File.OpenRead(filePath) ) )
			{
				TS3Pak readPAK = new TS3Pak( );

				if ( !Enumerable.SequenceEqual( reader.ReadBytes( 4 ), magicNumber ) )
				{
					throw new IOException( "Malformed PAK (Bad Header)" );
				}

				readPAK.info_offset = reader.ReadInt32( );
				readPAK.info_length = reader.ReadInt32( );

				reader.BaseStream.Seek( readPAK.info_offset, SeekOrigin.Begin );

				for ( int i=1; i<=readPAK.FileCount; i++ )
				{
					TS3Pak_File file = new TS3Pak_File( );

					file.FileCRC = reader.ReadUInt32( );
					file.FilePos = reader.ReadInt32( );
					file.FileLength = reader.ReadInt32( );
					file.FileCompressedLength = reader.ReadInt32( );

					long oldPos = reader.BaseStream.Position;

					reader.BaseStream.Seek( file.FilePos, SeekOrigin.Begin );

					if ( file.FileCompressedLength > 0 )
					{
						using ( var stream_compressed = new MemoryStream( reader.ReadBytes( file.FileLength ) ) )
						using ( var stream_zip = new GZipStream( stream_compressed, CompressionMode.Decompress ) )
						using ( var stream_result = new MemoryStream( ) )
						{
							stream_zip.CopyTo( stream_result );
							file.FileData = stream_result.ToArray( );
						}
					}
					else
					{
						file.FileData = reader.ReadBytes( file.FileLength );
					}

					readPAK.file_list.Add( file );

					reader.BaseStream.Seek( oldPos, SeekOrigin.Begin );
				}
				return readPAK;
			}
		}

		public static TS3Pak ConstructFromFolder( string folderPath )
		{
			TS3Pak constructedPAK = new TS3Pak( );

			var dir = Directory.EnumerateFiles(folderPath, "*.*", SearchOption.AllDirectories);

			constructedPAK.info_length = dir.Count( ) * 16;

			Crc32 crc = new Crc32();

			//We're not actually writing anything when constructing from a folder so to fill in the FilePos field we
			//need to figure out the position the file would be written at manually using the sizes of the files
			//we're processing.
			int lengthSoFar = headerLen;
			foreach ( string filePath in dir )
			{
				string relativePath = TS3Extensions.GetRelativePath( filePath, folderPath );

				TS3Pak_File file = new TS3Pak_File( );

				file.FileCRC = crc.Get( Encoding.ASCII.GetBytes( relativePath ) );

				file.FilePos = lengthSoFar;
				file.FileLength = (int)(new FileInfo( filePath ).Length);

				//Byte alignment is a pain
				lengthSoFar += file.FileLength;
				lengthSoFar += (int)( -lengthSoFar % ByteAlignment + ByteAlignment ) % ByteAlignment;

				file.FileCompressedLength = 0;

				file.FileData = File.ReadAllBytes( filePath );

				constructedPAK.file_list.Add( file );
			}

			constructedPAK.info_offset = lengthSoFar;

			return constructedPAK;
		}
#endregion

#region Writing
		public void WriteToFile( string outPath )
		{
			using ( BinaryWriter bw = new BinaryWriter( File.Open(outPath, FileMode.Create) ))
			{
				bw.Write( magicNumber );
				bw.Write( info_offset );
				bw.Write( info_length );
				bw.Write( 0 ); //Unused?

				//Write file data
				foreach ( TS3Pak_File file in file_list )
				{
					bw.Seek( file.FilePos, SeekOrigin.Begin );
					bw.Write( file.FileData );
				}

				bw.Align( ByteAlignment );

				foreach( TS3Pak_File file in file_list )
				{
					bw.Write( file.FileCRC );
					bw.Write( file.FilePos );
					bw.Write( file.FileLength );
					bw.Write( file.FileCompressedLength );
				}
			}
		}
#endregion
	}
}
