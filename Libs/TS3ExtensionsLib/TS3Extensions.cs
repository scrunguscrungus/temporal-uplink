﻿using System;
using System.IO;
using System.Text;

namespace TS3ExtensionsLib
{
	/////
	/// TS3ExtensionsLib - Library containing extension methods to assist with data handling.
	/////

	public enum TS3_Endian
	{
		LITTLE_ENDIAN = 0,
		BIG_ENDIAN
	}

	public static class TS3Extensions
	{
		public static string GetRelativePath(string path, string root)
		{
			return path.Replace(root, string.Empty).Replace("\\", "/").Trim('/');
		}


		//Extensions
		public static void Align(this BinaryWriter bw, int alignment)
		{
			bw.Seek((int)(-bw.BaseStream.Position % alignment + alignment) % alignment, SeekOrigin.Current);
		}

		public static string ReadNTString(this BinaryReader br)
		{
			int stringLength = 1;
			long startpos = br.BaseStream.Position;

			while ( br.PeekChar() != '\0')
			{
				stringLength++;
				br.BaseStream.Seek(1, SeekOrigin.Current);
			}
			br.BaseStream.Seek(startpos, SeekOrigin.Begin);

			return Encoding.Default.GetString(br.ReadBytes(stringLength));
		}

		//BinaryReader
		public static byte[] ReadBytes(this BinaryReader br, int bytes, TS3_Endian endianness)
		{
			byte[] result = br.ReadBytes(bytes);

			if (endianness == TS3_Endian.BIG_ENDIAN)
			{
				Array.Reverse(result);
			}

			return result;
		}

		public static int ReadInt32(this BinaryReader br, TS3_Endian endianness)
		{
			byte[] bytes = br.ReadBytes(4, endianness);

			return BitConverter.ToInt32(bytes, 0);
		}

		public static uint ReadUInt32(this BinaryReader br, TS3_Endian endianness)
		{
			byte[] bytes = br.ReadBytes(4, endianness);

			return BitConverter.ToUInt32(bytes, 0);
		}

		//BinaryWriter
		public static void Write(this BinaryWriter bw, int value, TS3_Endian endianness)
		{
			byte[] bytes = BitConverter.GetBytes(value);

			if (endianness == TS3_Endian.BIG_ENDIAN)
			{
				Array.Reverse(bytes);
			}

			bw.Write(bytes);
		}
	}
}
